## Importation des modules

from binarytree import Node


## Déclaration des classes


## Déclaration des fonctions

def est_vide(arbre):
    if arbre == None:
        return True
    else:
        return False

def est_une_feuille(noeud):
    if noeud.left == None and noeud.right == None:
        return True
    else:
        return False    



## Programme principal

arbre = Node(62)
arbre.left = Node(48)
arbre.right = Node(80)
arbre.left.left = Node(44)
arbre.left.right = Node(52)
arbre.left.left.left = Node(38)

arbre.right.left = Node(67)
arbre.right.right = Node(98)
arbre.right.right.left = Node(97)

print(arbre)
print(arbre.is_bst)