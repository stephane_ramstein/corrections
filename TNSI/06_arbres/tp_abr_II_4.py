## Importation des modules

from binarytree import Node, build


## Déclaration des classes


## Déclaration des fonctions

def est_vide(arbre):
    if arbre == None:
        return True
    else:
        return False

def est_une_feuille(noeud):
    if noeud.left == None and noeud.right == None:
        return True
    else:
        return False    

def recherche_ABR(arbre, etiquette):
    if est_vide(arbre):
        return False
    elif etiquette < arbre.value:
        return recherche_ABR(arbre.left, etiquette)
    elif etiquette > arbre.value:
        return recherche_ABR(arbre.right, etiquette)
    else:
        return True

## Programme principal

liste_valeurs = [7, 2, 28, 0, 4, 9, 29, None, 1, 3, 5, 8, 12, None, 30, None, None, None, None, 
None, None, None, 6, None, None, 10, 13]

arbre = build(liste_valeurs)
print(arbre)

print(recherche_ABR(arbre, 13))
print(recherche_ABR(arbre, 44))