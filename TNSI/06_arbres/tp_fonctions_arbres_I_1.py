## Importation des modules

from binarytree import Node


## Déclaration des classes


## Déclaration des fonctions

def est_vide(arbre):
    if arbre == None:
        return True
    else:
        return False

def est_une_feuille(noeud):
    if noeud.left == None and noeud.right == None:
        return True
    else:
        return False    


## Programme principal

arbre_vide = None

arbre = Node(1)
arbre.left = Node(10)
arbre.right = Node(6)
arbre.left.left = Node(11)
arbre.left.right = Node(8)
arbre.left.left.right = Node(5)
arbre.right.left = Node(9)
arbre.right.left.right = Node(13)
arbre.right.left.right.left = Node(14)
arbre.right.left.right.right = Node(3)
arbre.right.left.right.left.left = Node(7)
arbre.right.left.right.left.right = Node(4)
arbre.right.left.right.left.left.left = Node(2)
arbre.right.left.right.right = Node(3)
arbre.right.left.right.right.right = Node(12)

print(arbre)

print(est_vide(arbre_vide))
print(est_vide(arbre))

print(arbre.left.value)
print(est_une_feuille(arbre.left))

print(arbre.left.left.right.value)
print(est_une_feuille(arbre.left.left.right))