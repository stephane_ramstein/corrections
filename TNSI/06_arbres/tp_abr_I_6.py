## Importation des modules

from binarytree import Node


## Déclaration des classes


## Déclaration des fonctions

def est_vide(arbre):
    if arbre == None:
        return True
    else:
        return False

def est_une_feuille(noeud):
    if noeud.left == None and noeud.right == None:
        return True
    else:
        return False    

def parcours_prefixe(arbre):
    if est_vide(arbre) == True:
        return []
    else:
        return [arbre.value] + parcours_prefixe(arbre.left) + parcours_prefixe(arbre.right)

def parcours_infixe(arbre):
    if est_vide(arbre) == True:
        return []
    else:
        return parcours_infixe(arbre.left) + [arbre.value] + parcours_infixe(arbre.right)

def parcours_infixe_droite_gauche(arbre):
    if est_vide(arbre) == True:
        return []
    else:
        return parcours_infixe_droite_gauche(arbre.right) + [arbre.value] + parcours_infixe_droite_gauche(arbre.left)

def parcours_postfixe(arbre):
    if est_vide(arbre) == True:
        return []
    else:
        return parcours_postfixe(arbre.left) + parcours_postfixe(arbre.right) + [arbre.value]
  
def parcours_largeur(arbre):
    liste = []
    file = []
    file.append(arbre)
    while file != []:
        noeud = file.pop(0)
        if est_vide(noeud) != True:
            liste.append(noeud.value)
            file.append(noeud.left)
            file.append(noeud.right)
            
    return liste


## Programme principal

arbre = Node(62)
arbre.left = Node(48)
arbre.right = Node(80)
arbre.left.left = Node(44)
arbre.left.right = Node(52)
arbre.left.left.left = Node(38)

arbre.right.left = Node(67)
arbre.right.right = Node(98)
arbre.right.right.left = Node(97)

print(arbre)
print(arbre.is_bst)

print(parcours_infixe(arbre))
print(parcours_infixe_droite_gauche(arbre))
