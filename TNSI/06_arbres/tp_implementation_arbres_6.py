## Importation des modules

from binarytree import Node


## Déclaration des classes


## Déclaration des fonctions


## Programme principal

noeud_A = Node("A", None, None)
noeud_E = Node("E", None, None)
noeud_I = Node("I", None, None)
noeud_K = Node("K", None, None)
noeud_L = Node("L", None, None)
noeud_J = Node("J", None, noeud_K)
noeud_B = Node("B", noeud_A, None)
noeud_D = Node("D", noeud_J, noeud_E)
noeud_H = Node("H", noeud_L, noeud_I)
noeud_C = Node("C", noeud_B, noeud_D)
noeud_G = Node("G", None, noeud_H)
noeud_F = Node("F", noeud_C, noeud_G)
arbre1 = noeud_F

arbre2 = Node('manchot')
arbre2.left = Node('poule')
arbre2.left.left = Node('chat')
arbre2.left.left.left = Node('rat')
arbre2.left.left.right = Node('chien')
arbre2.left.right = Node('pigeon')
arbre2.right = Node('canari')
arbre2.right.left = Node('léopard')
arbre2.right.right = Node('vache')
arbre2.right.right.right = Node('gazelle')

arbre3 = Node(7, Node(11, None, Node(2, None, None)), Node(1, Node(9, None, None), Node(13, None, None)))

print(arbre1)
print(arbre2)
print(arbre3)

