## Importation des modules


## Déclaration des classes

class Node:
    '''
    Classe permettant de créer un objet Node (noeud).
    
    @attribut : value, valeur de l’étiquette du noeud, de type quelconque, pas de valeur par défaut.
    @attribut : left : noeud suivant gauche, de type Node, None par défaut.
    @attribut : right : noeud suivant droit, de type Node, None par défaut.
    '''
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


## Déclaration des fonctions


## Programme principal

noeud_A = Node("A", None, None)
noeud_E = Node("E", None, None)
noeud_I = Node("I", None, None)
noeud_K = Node("K", None, None)
noeud_L = Node("L", None, None)
noeud_J = Node("J", None, noeud_K)
noeud_B = Node("B", noeud_A, None)
noeud_D = Node("D", noeud_J, noeud_E)
noeud_H = Node("H", noeud_L, noeud_I)
noeud_C = Node("C", noeud_B, noeud_D)
noeud_G = Node("G", None, noeud_H)
noeud_F = Node("F", noeud_C, noeud_G)
arbre = noeud_F

print(arbre)
print(arbre.value)
print(arbre.left.value)
print(arbre.right.value)
print(arbre.left.right.left.value)