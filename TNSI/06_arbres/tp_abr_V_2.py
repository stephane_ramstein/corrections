## Importation des modules

from binarytree import Node, build


## Déclaration des classes


## Déclaration des fonctions

def est_vide(arbre):
    if arbre == None:
        return True
    else:
        return False

def est_une_feuille(noeud):
    if noeud.left == None and noeud.right == None:
        return True
    else:
        return False    

def recherche_ABR(arbre, etiquette):
    if est_vide(arbre):
        return False
    elif etiquette < arbre.value:
        return recherche_ABR(arbre.left, etiquette)
    elif etiquette > arbre.value:
        return recherche_ABR(arbre.right, etiquette)
    else:
        return True

def min_ABR(arbre):
    if est_vide(arbre.left):
        return arbre.value
    else:
        return min_ABR(arbre.left)

def max_ABR(arbre):
    if est_vide(arbre.right):
        return arbre.value
    else:
        return max_ABR(arbre.right)

def inserer_ABR(arbre, etiquette):
    if est_vide(arbre):
        return Node(etiquette)
    elif etiquette <= arbre.value:
#         nouvel_arbre_gauche = inserer_ABR(arbre.left, etiquette)
#         return Node(arbre.value, nouvel_arbre_gauche, arbre.right)
        return Node(arbre.value, inserer_ABR(arbre.left, etiquette), arbre.right)
    else:
#         nouvel_arbre_droite = inserer_ABR(arbre.right, etiquette)
#         return Node(arbre.value, arbre.left, nouvel_arbre_droite)
        return Node(arbre.value, arbre.left, inserer_ABR(arbre.right, etiquette))

def supprimer_ABR(arbre, etiquette):
    if est_vide(arbre) == True:
        return None
    elif etiquette < arbre.value:
        return Node(arbre.value, supprimer_ABR(arbre.left, etiquette), arbre.right)
    elif etiquette > arbre.value:
        return Node(arbre.value, arbre.left, supprimer_ABR(arbre.right, etiquette))
    else:
        print("Noeud trouvé. Il faut le supprimer et reconstruire l'arbre")
        return arbre

## Programme principal

liste_valeurs = [7, 2, 28, 0, 4, 9, 29, None, 1, 3, 5, 8, 12, None, 30, None, None, None, None, 
None, None, None, 6, None, None, 10, 13]

arbre = build(liste_valeurs)
print(arbre)

arbre = supprimer_ABR(arbre, 12)
# print(arbre)