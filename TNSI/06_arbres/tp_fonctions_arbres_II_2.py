## Importation des modules

from binarytree import Node


## Déclaration des classes


## Déclaration des fonctions

def est_vide(arbre):
    if arbre == None:
        return True
    else:
        return False

def est_une_feuille(noeud):
    if noeud.left == None and noeud.right == None:
        return True
    else:
        return False    

def taille(arbre):
    if est_vide(arbre) == True:
        return 0
    else:
        return 1 + taille(arbre.left) + taille(arbre.right)

def hauteur(arbre):
    if est_vide(arbre) == True:
        return -1
    else:
#         hauteur_gauche = hauteur(arbre.left)
#         hauteur_droit = hauteur(arbre.right)
#         if hauteur_gauche > hauteur_droit:
#             return 1 + hauteur_gauche
#         else:
#             return 1 + hauteur_droit
        
        return 1 + max(hauteur(arbre.left), hauteur(arbre.right))

def profondeur(arbre, etiquette):
    if est_vide(arbre) == True:
        return None
    elif arbre.value == etiquette:
        return 0
    else:
        profondeur_gauche = profondeur(arbre.left, etiquette)
        profondeur_droit = profondeur(arbre.right, etiquette)
        
        if profondeur_gauche != None:
            return profondeur_gauche + 1
        elif profondeur_droit != None:
            return profondeur_droit + 1
        else:
            return None

def parcours_prefixe(arbre):
    if est_vide(arbre) == True:
        return None
    else:
        print(arbre.value)
        parcours_prefixe(arbre.left)
        parcours_prefixe(arbre.right)

def parcours_infixe(arbre):
    if est_vide(arbre) == True:
        return None
    else:
        parcours_infixe(arbre.left)
        print(arbre.value)
        parcours_infixe(arbre.right)

def parcours_postfixe(arbre):
    if est_vide(arbre) == True:
        return None
    else:
        parcours_postfixe(arbre.left)
        parcours_postfixe(arbre.right)
        print(arbre.value)


## Programme principal

arbre_vide = None

arbre = Node(1)
arbre.left = Node(10)
arbre.right = Node(6)
arbre.left.left = Node(11)
arbre.left.right = Node(8)
arbre.left.left.right = Node(5)
arbre.right.left = Node(9)
arbre.right.left.right = Node(13)
arbre.right.left.right.left = Node(14)
arbre.right.left.right.right = Node(3)
arbre.right.left.right.left.left = Node(7)
arbre.right.left.right.left.right = Node(4)
arbre.right.left.right.left.left.left = Node(2)
arbre.right.left.right.right = Node(3)
arbre.right.left.right.right.right = Node(12)

print('Parcours prefixe')
parcours_prefixe(arbre)
print()

print('Parcours infixe')
parcours_infixe(arbre)
print()

print('Parcours postfixe')
parcours_postfixe(arbre)
