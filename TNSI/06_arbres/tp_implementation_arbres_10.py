## Importation des modules

from binarytree import Node


## Déclaration des classes


## Déclaration des fonctions


## Programme principal

noeud_3 = Node(3, None, None)
noeud_8 = Node(8, None, None)
noeud_0 = Node(0, None, None)
noeud_11 = Node(11, None, None)
noeud_13 = Node(13, None, None)
noeud_6 = Node(6, noeud_3, noeud_8)
noeud_1 = Node(1, noeud_6, noeud_0)
noeud_2 = Node(2, None, noeud_11)
noeud_7 = Node(7, None, noeud_13)
noeud_6 = Node(5, noeud_2, noeud_7)
noeud_5 = Node(5, noeud_2, noeud_7)
noeud_12 = Node(12, noeud_1, noeud_5)
arbre1 = noeud_12

print(arbre1)
print(arbre1.is_complete)

noeud_3 = Node(3, None, None)
noeud_8 = Node(8, None, None)
noeud_0 = Node(0, None, None)
noeud_6 = Node(6, noeud_3, noeud_8)
noeud_1 = Node(1, noeud_6, noeud_0)
noeud_2 = Node(2, None, None)
noeud_7 = Node(7, None, None)
noeud_6 = Node(5, noeud_2, noeud_7)
noeud_5 = Node(5, noeud_2, noeud_7)
noeud_12 = Node(12, noeud_1, noeud_5)
arbre2 = noeud_12

print(arbre2)
print(arbre2.is_complete)
