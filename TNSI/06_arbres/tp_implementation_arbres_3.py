## Importation des modules


## Déclaration des classes

class Node:
    '''
    Classe permettant de créer un objet Node (noeud).
    
    @attribut : value, valeur de l’étiquette du noeud, de type quelconque, pas de valeur par défaut.
    @attribut : left : noeud suivant gauche, de type Node, None par défaut.
    @attribut : right : noeud suivant droit, de type Node, None par défaut.
    '''
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


## Déclaration des fonctions


## Programme principal

arbre = Node(7, Node(11, None, Node(2, None, None)), Node(1, Node(9, None, None), Node(13,
None, None)))

print(arbre)
print(arbre.value)
print(arbre.left.value)
print(arbre.right.value)
