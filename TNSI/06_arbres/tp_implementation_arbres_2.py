## Importation des modules


## Déclaration des classes

class Node:
    '''
    Classe permettant de créer un objet Node (noeud).
    
    @attribut : value, valeur de l’étiquette du noeud, de type quelconque, pas de valeur par défaut.
    @attribut : left : noeud suivant gauche, de type Node, None par défaut.
    @attribut : right : noeud suivant droit, de type Node, None par défaut.
    '''
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


## Déclaration des fonctions


## Programme principal

arbre = Node('manchot')
arbre.left = Node('poule')
arbre.left.left = Node('chat')
arbre.left.left.left = Node('rat')
arbre.left.left.right = Node('chien')
arbre.left.right = Node('pigeon')
arbre.right = Node('canari')
arbre.right.left = Node('léopard')
arbre.right.right = Node('vache')
arbre.right.right.right = Node('gazelle')

print(arbre)
print(arbre.value)
print(arbre.left.value)
print(arbre.right.value)
print(arbre.left.left.right.value)