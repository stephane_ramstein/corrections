## Importation des modules

import math


## Déclaration des fonctions

def somme(n):
    '''
    Calcule la somme des entiers de 0 à n inclus.
    
    @param_entree : n, nombre entier
    @param_sortie : accumulateur, nombre entier
    '''
    
    assert type(n) == int, 'le paramètre doit être un entier'
    assert n > 0, 'le paramètre doit être strictement positif'

    accumulateur = 0
    for i in range(n + 1):
        accumulateur = accumulateur + i

    return accumulateur

def factorielle(n):
    '''
    Calcule le produit des entiers de 1 à n inclus.
    
    @param_entree : n, nombre entier
    @param_sortie : accumulateur, nombre entier
    '''

    assert type(n) == int, 'le paramètre doit être un entier'
    assert n >= 1, 'le paramètre doit être supérieur ou égal à 1'

    accumulateur = 1
    for i in range(1, n + 1):
        accumulateur = accumulateur * i
        
    return accumulateur

def multiplication(a, b):
    '''
    Calcule le produit de a par b, a et b sont des entiers positifs.
    
    @param_entree : a, nombre entier
    @param_entree : b, nombre entier
    @param_sortie : c, nombre entier
    '''

    assert type(a) == int, 'le premier paramètre doit être un entier'
    assert type(b) == int, 'le second paramètre doit être un entier'
    assert a >= 0, 'le premier paramètre doit être positif'
    assert b >= 0, 'le second paramètre doit être positif'
    
    c = 0
    for i in range(b):
        c = c + a

    return c

def division(a, b):
    '''
    Calcule le quotient et le reste de la division euclidienne de a par b.
    
    @param_entree : a, nombre entier
    @param_entree : b, nombre entier
    @param_sortie : q, nombre entier
    @param_sortie : r, nombre entier

    '''

    r = a
    q = 0
    while r >= b:
        r = r - b
        q = q + 1

    return q, r


def est_pair(n):
    '''
    Test si l'entier est pair.
    
    @param_entree : n, nombre entier
    @param_sortie : test, booléen

    '''
    
    if n % 2 == 0:
        test = True
    else:
        test = False
        
    return test

def est_impair(n):
    '''
    Test si l'entier est pair.
    
    @param_entree : n, nombre entier
    @param_sortie : test, booléen

    '''
    
    if n % 2 == 1:
        test = True
    else:
        test = False

# Fait la même chose :
#     if n % 2 == 0:
#         test = False
#     else:
#         test = True
        
    return test

def est_premier(a):
    '''
    Teste si un nombre est premier.
    
    @param_entree : a, nombre entier
    @param_sortie : test, valeur booléenne
    '''

    if a <= 1:
        test = False
    else:
        test = True
        for valeur in range(2, a):
            if a % valeur == 0:
                test = False
    
    return test

def est_premier_moins_naif(a):
    '''
    Teste si un nombre est premier.
    
    @param_entree : a, nombre entier
    @param_sortie : test, valeur booléenne
    '''

    if a <= 1:
        test = False
    else:
        test = True
        for valeur in range(2, int(math.sqrt(a))):
            if a % valeur == 0:
                test = False
    
    return test

def pgcd(a, b):
    '''
    Retourne le Plus Grand Commun Diviseur de de entiers a et b.

    @param_entree : a, nombre entier
    @param_entree : b, nombre entier
    @param_sortie : a, nombre entier
    '''

    while b != 0:
        temp = b
        b = a % b
        a = temp
        
    return a

def somme_tableau(tableau):
    '''
    Effectue la somme des éléments d'un tableau.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : accumulateur, nombre entier ou réel
    '''

#     accumulateur = 0
#     for element in tableau:
#         accumulateur = accumulateur + element
    
    n = len(tableau)
    
    accumulateur = 0
    for i in range(n):
        accumulateur = accumulateur + tableau[i]
        
    return accumulateur

def moyenne_tableau(tableau):
    '''
    Effectue la moyenne des éléments d'un tableau.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''

#     n = len(tableau)
# 
#     accumulateur = 0
#     for element in tableau:
#         accumulateur = accumulateur + element
#         
#     moyenne = accumulateur / n
    
    n = len(tableau)
    
    accumulateur = 0
    for i in range(n):
        accumulateur = accumulateur + tableau[i]
    
    moyenne = accumulateur / n
#     
    return moyenne

def recherche_minimum(tableau):
    '''
    Recherche le plus petit élément d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_minimum
    '''

    n = len(tableau)
    valeur_minimum = tableau[0]

    for i in range(1, n):
        if tableau[i] < valeur_minimum:
            valeur_minimum = tableau[i]

    return valeur_minimum

def recherche_maximum(tableau):
    '''
    Recherche le plus grand élément d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_maximum
    '''

    n = len(tableau)
    valeur_maximum = tableau[0]

    for i in range(1, n):
        if tableau[i] > valeur_maximum:
            valeur_maximum = tableau[i]

    return valeur_maximum

def recherche_premiere_occurence(tableau, element):
    '''
    Retourne la position de la première occurence de l'élément dans le tableau.
    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''

    n = len(tableau)
    position = None
    
    i = 0
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
#             i += 1

    return position

def recherche_toutes_occurences(tableau, element):
    '''
    Retourne la position de toutes les occurences de l'élément dans le tableau.
    @param_entree : tableau, liste de valeurs
    @param_entree : element, valeur
    @param_sortie : positions, liste d'entiers
    '''

    n = len(tableau)
    positions = []
    
    for i in range(n):
        if tableau[i] == element:
            positions.append(i)

    return positions


## Programme principal

# resultat = somme(-3)
# print(resultat)

# print(somme(3))

# resultat = factorielle(3)
# print(resultat)

# resultat = multiplication(3, 5)
# print(resultat)

# resultat = division(20, 6)
# print(resultat)

# resultat = est_pair(10)
# print(resultat)
# 
# resultat = est_impair(10)
# print(resultat)

# resultat = est_premier(3)
# print(resultat)
# 
# resultat = est_premier_moins_naif(3)
# print(resultat)

# resultat = pgcd(10, 15)
# print(resultat)

# resultat = somme_tableau([5, 9, 4, 3, 7, 9])
# print(resultat)

# resultat = moyenne_tableau([5, 9, 4, 3, 7, 9])
# print(resultat)

# resultat = recherche_minimum([5, 9, 4, 3, 7, 9])
# print(resultat)

# resultat = recherche_maximum([5, 9, 4, 3, 7, 9])
# print(resultat)

# resultat = recherche_premiere_occurence([5, 9, 4, 3, 7, 9], 3)
# print(resultat)

resultat = recherche_toutes_occurences([5, 9, 4, 3, 7, 9], 9)
print(resultat)