## Importation des modules


## Déclaration des classes

class Voiture:
    '''Une classe définissant un modèle de voitures'''
    
    def __init__(self, param_moteur, param_couleur, param_puissance):
        self.moteur = param_moteur
        self.couleur = param_couleur
        self.puissance = param_puissance
        
    def caracteristiques(self):
        return {"moteur": self.moteur, "couleur": self.couleur, "puissance": self.puissance}

    def modifie_couleur(self, nouvelle_couleur):
        self.couleur = nouvelle_couleur
    
    def __del__(self):
        print('Non pitié, ne me tuez pas !!!!!!!!!!!!!')


## Déclaration des fonctions


## Programme principal
        
voiture_1 = Voiture('essence', 'bleue', 150)
voiture_2 = Voiture('électrique', 'jaune', 110)

print(voiture_1.moteur)
print(voiture_1.couleur)
print(voiture_1.puissance)
print(voiture_2.moteur)
print(voiture_2.couleur)
print(voiture_2.puissance)