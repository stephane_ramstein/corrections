## Importation des modules


## Déclaration des classes

class Fleur:
    def __init__(self, param_couleur=['blanc'], param_taille=20, param_nbr_petales=10, param_nbr_feuilles=0):
        self.couleur = param_couleur
        self.taille = param_taille
        self.nbr_petales = param_nbr_petales
        self.nbr_feuilles = param_nbr_feuilles
    
    def affiche_caracteristiques(self):
        print('Les caractéristiques de la fleur sont :')
        print('  couleur :', self.couleur)
        print('  taille :', self.taille)
        print('  nbr_petales :', self.nbr_petales)
        print('  nbr_feuilles :', self.nbr_feuilles)
        
    def ajoute_petale(self):
        self.nbr_petales = self.nbr_petales + 1

    def enleve_petale(self):
        if self.nbr_petales != 0:
            self.nbr_petales = self.nbr_petales - 1

    def allonge(self, x):
        assert x >= 0, "le paramètre doit être positif ou nul."
        
        self.taille = self.taille + x
        
    def pousse(self):
        self.taille = self.taille * 1.1
#         self.taille = self.taille + self.taille * 10 / 100

    def est_fanee(self):
#         if self.nbr_petales == 0:
#             return True
#         else:
#             return False
        
        return self.nbr_petales == 0
    
    def croisement(self, autre_fleur):
        nouvelle_fleur = Fleur()
        nouvelle_fleur.couleur = self.couleur + autre_fleur.couleur
        nouvelle_fleur.taille = (self.taille + autre_fleur.taille) / 2
        nouvelle_fleur.nbr_petales = (self.nbr_petales + autre_fleur.nbr_petales) // 2
        nouvelle_fleur.nbr_feuilles = (self.nbr_feuilles + autre_fleur.nbr_feuilles) // 2
        
        return nouvelle_fleur      


## Déclaration des fonctions


## Programme principal

fleur_1 = Fleur(param_couleur=['rouge'])
fleur_2 = Fleur(param_taille=30, param_nbr_petales=4)

fleur_1.affiche_caracteristiques()
print()
fleur_2.affiche_caracteristiques()
print()

fleur_3 = fleur_1.croisement(fleur_2)
fleur_3.affiche_caracteristiques()
