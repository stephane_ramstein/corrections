## Importation des modules


## Déclaration des classes

class Voiture:
    '''Une classe définissant un modèle de voitures'''
    
    def __init__(self, param_moteur, param_couleur, param_puissance, param_nombre_portes, param_masse, param_boite_vitesse, param_masse_vide, param_masse_PTAC):
        self.moteur = param_moteur
        self.couleur = param_couleur
        self.puissance = param_puissance
        self.nombre_portes = param_nombre_portes
        self.masse = param_masse
        self.boite_vitesse = param_boite_vitesse
        self.masse_vide = param_masse_vide
        self.masse_PTAC = param_masse_PTAC
        
    def caracteristiques(self):
        return {"moteur": self.moteur, "couleur": self.couleur, "puissance": self.puissance}

    def modifie_moteur(self, nouveau_moteur):
        self.moteur = nouveau_moteur

    def modifie_couleur(self, nouvelle_couleur):
        self.couleur = nouvelle_couleur

    def modifie_puissance(self, nouvelle_puissance):
        self.puissance = nouvelle_puissance
    
    def __del__(self):
        print('Non pitié, ne me tuez pas !!!!!!!!!!!!!')


## Déclaration des fonctions


## Programme principal
        
voiture_1 = Voiture('essence', 'bleu', 150, 5, 550, 'manuelle', 400, 1000)
voiture_2 = Voiture('électrique', 'jaune', 110, 3, 400, 'automatique', 300, 800)
