## Importation des modules

from binarytree import Node
import pickle


## Déclaration des fonctions


## Programme principa

arbre = Node('test')

print(arbre)

fichier = open('fichier_test' + '.obj', 'wb')
pickle.dump(arbre, fichier)
fichier.close()

fichier = open('fichier_test' + '.obj', 'rb')
arbre = pickle.load(fichier)
fichier.close()

print(arbre)