## Importation des modules

import time


## Déclaration des fonctions

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)

def puissance(x, n):
    if n == 0:
        return 1
    else:
        return x * puissance(x, n - 1)
    
# def puissance(x, n):
#     if n == 0:
#         return 1
#     elif n == 1:
#         return x
#     else:
#         return x * puissance(x, n - 1)

# def puissance(x, n):
#     if n == 0:
#         return 1
#     else:
#         if n % 2 == 0:
#             return (puissance(x, n // 2)) ** 2
#         else:
#             return  x * (puissance(x, (n - 1) // 2)) ** 2

# def puissance(x, n):
#     if n == 0:
#         return 1
#     elif n > 0 and n % 2 == 0:
#         return (puissance(x, n // 2)) ** 2
#     else:
#         return  x * (puissance(x, (n - 1) // 2)) ** 2

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

def padovan(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 1
    else:
        return padovan(n - 2) + padovan(n - 3)


## Programme principal

print(padovan(0))
print(padovan(1))
print(padovan(2))
print(padovan(3))
print(padovan(4))
print(padovan(5))
print(padovan(6))
print(padovan(7))
print(padovan(8))
print(padovan(9))
print(padovan(10))
print(padovan(11))
print(padovan(12))
