## Importation des modules

import turtle


## Déclaration des fonctions

def koch(n, l):
    if n == 0:
        turtle.forward(l)
    else:
        koch(n - 1, l // 3)
        turtle.left(60)
        koch(n - 1, l // 3)
        turtle.right(120)
        koch(n - 1, l // 3)
        turtle.left(60)
        koch(n - 1, l // 3)

def flocon(n, l):
    koch(n, l)
    turtle.right(120)
    koch(n, l)
    turtle.right(120)
    koch(n, l)


## Programme principal

turtle.speed(0)
turtle.hideturtle()

turtle.penup()
turtle.backward(200)
turtle.pendown()

flocon(3, 300)

turtle.done()