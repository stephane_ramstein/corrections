## Importation des modules

import turtle


## Déclaration des fonctions

def koch(n, l):
    if n == 0:
        turtle.forward(l)
    else:
        koch(n - 1, l // 3)
        turtle.left(60)
        koch(n - 1, l // 3)
        turtle.right(120)
        koch(n - 1, l // 3)
        turtle.left(60)
        koch(n - 1, l // 3)
    

## Programme principal

turtle.speed(0)

turtle.penup()
turtle.backward(200)
turtle.pendown()

koch(4, 300)

turtle.done()