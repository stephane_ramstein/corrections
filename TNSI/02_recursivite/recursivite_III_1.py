## Importation des modules


## Déclaration des fonctions

def recherche_dichotomique(tableau, element_cherche):
    n = len(tableau)
    
    if n == 1:
        return 0
    else:
        i_milieu = n // 2
        
        if element_cherche >= tableau[i_milieu]:
            return i_milieu + recherche_dichotomique(tableau[i_milieu:], element_cherche)
        else:
            return 0 + recherche_dichotomique(tableau[:i_milieu], element_cherche)


## Programme principal

tableau = [1, 5, 6, 7, 8, 10, 13, 14, 17, 19]

resultat = recherche_dichotomique(tableau, 7)
print(resultat)
