## Importation des modules


## Déclaration des fonctions

def hanoi(n, depart, arrivee, intermediaire):
    if n == 1:
        liste_mouvements.append(depart + '->' + arrivee)
#         print(depart, '->', arrivee)
    else:
        hanoi(n - 1, depart, intermediaire, arrivee)
        hanoi(1, depart, arrivee, intermediaire)
        hanoi(n - 1, intermediaire, arrivee, depart)


## Programme principal

liste_mouvements = []

hanoi(8, 'T1', 'T3', 'T2')

print(liste_mouvements)
print(len(liste_mouvements))