## Importation des modules

import time


## Déclaration des fonctions

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)

def puissance(x, n):
    if n == 0:
        return 1
    else:
        return x * puissance(x, n - 1)
    
# def puissance(x, n):
#     if n == 0:
#         return 1
#     elif n == 1:
#         return x
#     else:
#         return x * puissance(x, n - 1)

# def puissance(x, n):
#     if n == 0:
#         return 1
#     else:
#         if n % 2 == 0:
#             return (puissance(x, n // 2)) ** 2
#         else:
#             return  x * (puissance(x, (n - 1) // 2)) ** 2

# def puissance(x, n):
#     if n == 0:
#         return 1
#     elif n > 0 and n % 2 == 0:
#         return (puissance(x, n // 2)) ** 2
#     else:
#         return  x * (puissance(x, (n - 1) // 2)) ** 2

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


## Programme principal

t1 = time.perf_counter()
puissance(4, 5)
t2 = time.perf_counter()

duree = t2 - t1
print("Temps mis par l'algorithme :", duree)