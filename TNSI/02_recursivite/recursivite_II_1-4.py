## Importation des modules

import turtle


## Déclaration des fonctions

def arbre(n, l, e):
    turtle.pensize(e)
    
    if n == 0:
        turtle.pencolor('red')
        turtle.forward(l)
        turtle.backward(l)
    else:
        turtle.pencolor('brown')
        turtle.forward(l)
        turtle.left(30)
        arbre(n - 1, l * 2 // 3, e * 2 // 3)
        turtle.right(60)
        arbre(n - 1, l * 2 // 3, e * 2 // 3)
        turtle.left(30)
        turtle.backward(l)


## Programme principal

turtle.penup()
turtle.left(90)
turtle.backward(200)
turtle.pendown()

arbre(5, 150, 4)

turtle.done()