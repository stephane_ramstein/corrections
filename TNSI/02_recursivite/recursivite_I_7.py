## Importation des modules

import time


## Déclaration des fonctions

def somme(n):
    if n == 0:
        return 0
    else:
        return n + somme(n - 1)

def puissance(x, n):
    if n == 0:
        return 1
    else:
        return x * puissance(x, n - 1)
    
# def puissance(x, n):
#     if n == 0:
#         return 1
#     elif n == 1:
#         return x
#     else:
#         return x * puissance(x, n - 1)

# def puissance(x, n):
#     if n == 0:
#         return 1
#     else:
#         if n % 2 == 0:
#             return (puissance(x, n // 2)) ** 2
#         else:
#             return  x * (puissance(x, (n - 1) // 2)) ** 2

# def puissance(x, n):
#     if n == 0:
#         return 1
#     elif n > 0 and n % 2 == 0:
#         return (puissance(x, n // 2)) ** 2
#     else:
#         return  x * (puissance(x, (n - 1) // 2)) ** 2

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


## Programme principal

print(fibonacci(0))
print(fibonacci(1))
print(fibonacci(2))
print(fibonacci(3))
print(fibonacci(4))
print(fibonacci(5))
print(fibonacci(6))
print(fibonacci(7))
print(fibonacci(8))
print(fibonacci(9))
print(fibonacci(10))
print(fibonacci(11))
print(fibonacci(12))
