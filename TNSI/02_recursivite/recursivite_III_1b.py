## Importation des modules


## Déclaration des fonctions

def recherche_dichotomique(tableau, element_cherche):
    n = len(tableau)
    
    if n == 1:
        if element_cherche == tableau[0]:
            return 0
        else:
            return None
    else:
        i_milieu = n // 2
        
        if element_cherche >= tableau[i_milieu]:
            i_sous_liste = recherche_dichotomique(tableau[i_milieu:], element_cherche)
            if i_sous_liste != None:
                return i_milieu + i_sous_liste
            else:
                return None
        else:
            i_sous_liste = recherche_dichotomique(tableau[:i_milieu], element_cherche)
            if i_sous_liste != None:
                return 0 + i_sous_liste
            else:
                return None


## Programme principal

tableau = [1, 5, 6, 7, 8, 10, 13, 14, 17, 19]

resultat = recherche_dichotomique(tableau, 7)
print(resultat)
