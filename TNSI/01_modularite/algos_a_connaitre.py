'''
Ce module contient un grande partie des fonctions à connaître en TNSI.
'''

## Importation des modules

import math


## Déclaration des fonctions

def somme(n):
    '''
    Calcule la somme des entiers de 0 à n inclus.
    
    @param_entree : n, nombre entier
    @param_sortie : accumulateur, nombre entier
    '''
    
    assert type(n) == int, 'le paramètre doit être un entier'
    assert n > 0, 'le paramètre doit être strictement positif'

    accumulateur = 0
    for i in range(n + 1):
        accumulateur = accumulateur + i

    return accumulateur

def factorielle(n):
    '''
    Calcule le produit des entiers de 1 à n inclus.
    
    @param_entree : n, nombre entier
    @param_sortie : accumulateur, nombre entier
    '''

    assert type(n) == int, 'le paramètre doit être un entier'
    assert n >= 1, 'le paramètre doit être supérieur ou égal à 1'

    accumulateur = 1
    for i in range(1, n + 1):
        accumulateur = accumulateur * i
        
    return accumulateur

def multiplication(a, b):
    '''
    Calcule le produit de a par b, a et b sont des entiers positifs.
    
    @param_entree : a, nombre entier
    @param_entree : b, nombre entier
    @param_sortie : c, nombre entier
    '''

    assert type(a) == int, 'le premier paramètre doit être un entier'
    assert type(b) == int, 'le second paramètre doit être un entier'
    assert a >= 0, 'le premier paramètre doit être positif'
    assert b >= 0, 'le second paramètre doit être positif'
    
    c = 0
    for i in range(b):
        c = c + a

    return c

def division(a, b):
    '''
    Calcule le quotient et le reste de la division euclidienne de a par b.
    
    @param_entree : a, nombre entier
    @param_entree : b, nombre entier
    @param_sortie : q, nombre entier
    @param_sortie : r, nombre entier

    '''

    r = a
    q = 0
    while r >= b:
        r = r - b
        q = q + 1

    return q, r


def est_pair(n):
    '''
    Test si l'entier est pair.
    
    @param_entree : n, nombre entier
    @param_sortie : test, booléen

    '''
    
    if n % 2 == 0:
        test = True
    else:
        test = False
        
    return test

def est_impair(n):
    '''
    Test si l'entier est pair.
    
    @param_entree : n, nombre entier
    @param_sortie : test, booléen

    '''
    
    if n % 2 == 1:
        test = True
    else:
        test = False

# Fait la même chose :
#     if n % 2 == 0:
#         test = False
#     else:
#         test = True
        
    return test

def est_premier(a):
    '''
    Teste si un nombre est premier.
    
    @param_entree : a, nombre entier
    @param_sortie : test, valeur booléenne
    '''

    if a <= 1:
        test = False
    else:
        test = True
        for valeur in range(2, a):
            if a % valeur == 0:
                test = False
    
    return test

def est_premier_moins_naif(a):
    '''
    Teste si un nombre est premier.
    
    @param_entree : a, nombre entier
    @param_sortie : test, valeur booléenne
    '''

    if a <= 1:
        test = False
    else:
        test = True
        for valeur in range(2, int(math.sqrt(a))):
            if a % valeur == 0:
                test = False
    
    return test

def pgcd(a, b):
    '''
    Retourne le Plus Grand Commun Diviseur de de entiers a et b.

    @param_entree : a, nombre entier
    @param_entree : b, nombre entier
    @param_sortie : a, nombre entier
    '''

    while b != 0:
        temp = b
        b = a % b
        a = temp
        
    return a

def somme_tableau(tableau):
    '''
    Effectue la somme des éléments d'un tableau.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : accumulateur, nombre entier ou réel
    '''

#     accumulateur = 0
#     for element in tableau:
#         accumulateur = accumulateur + element
    
    n = len(tableau)
    
    accumulateur = 0
    for i in range(n):
        accumulateur = accumulateur + tableau[i]
        
    return accumulateur

def moyenne_tableau(tableau):
    '''
    Effectue la moyenne des éléments d'un tableau.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : moyenne, nombre entier ou réel
    '''

#     n = len(tableau)
# 
#     accumulateur = 0
#     for element in tableau:
#         accumulateur = accumulateur + element
#         
#     moyenne = accumulateur / n
    
    n = len(tableau)
    
    accumulateur = 0
    for i in range(n):
        accumulateur = accumulateur + tableau[i]
    
    moyenne = accumulateur / n
#     
    return moyenne

def recherche_minimum(tableau):
    '''
    Recherche le plus petit élément d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_minimum
    '''

    n = len(tableau)
    valeur_minimum = tableau[0]

    for i in range(1, n):
        if tableau[i] < valeur_minimum:
            valeur_minimum = tableau[i]

    return valeur_minimum

def recherche_maximum(tableau):
    '''
    Recherche le plus grand élément d'un tableau de valeurs.

    @param_entree : tableau, tableau de nombres entiers ou réels
    @param_sortie : valeur_maximum
    '''

    n = len(tableau)
    valeur_maximum = tableau[0]

    for i in range(1, n):
        if tableau[i] > valeur_maximum:
            valeur_maximum = tableau[i]

    return valeur_maximum

def recherche_premiere_occurence(tableau, element):
    '''
    Retourne la position de la première occurence de l'élément dans le tableau.
    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''

    n = len(tableau)
    position = None
    
    i = 0
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
#             i += 1

    return position

def recherche_toutes_occurences(tableau, element):
    '''
    Retourne la position de toutes les occurences de l'élément dans le tableau.
    @param_entree : tableau, liste de valeurs
    @param_entree : element, valeur
    @param_sortie : positions, liste d'entiers
    '''

    n = len(tableau)
    positions = []
    
    for i in range(n):
        if tableau[i] == element:
            positions.append(i)

    return positions

def insertion(liste, valeur_a_inserer, position):
    '''
    Insère l'élément valeur_a_inserer à l'indice position dans le tableau liste.

    @param_entree : liste, liste de nombres
    @param_entree : valeur_a_inserer, nombre
    @param_entree : position, nombre entier
    @param_sortie : liste (modifiée), liste de nombres
    '''

    liste.append(None)
    n = len(liste)

    i = n - 1
    while i >= position:
        liste[i] = liste[i - 1]
        i = i - 1
        
    liste[position] = valeur_a_inserer
    
    return liste

def tri_selection(tableau):
    '''
    Tri un tableau par la méthode de sélection.
    
    @param_entree : tableau (non_trié), liste de nombres
    @param_sortie : tableau (trié), liste de nombres
    '''
    
    n = len(tableau)

    for i in range(n):
        position_plus_petit = i
        for j in range(i, n):
            if tableau[j] < tableau[position_plus_petit]:
                position_plus_petit = j
        
        temp = tableau[i]
        tableau[i] = tableau[position_plus_petit]
        tableau[position_plus_petit] = temp
        
    return tableau

def tri_insertion(tableau):
    '''
    Tri un tableau par la méthode d'insertion.
    
    @param_entree : tableau (non_trié), liste de nombres
    @param_sortie : tableau (trié), liste de nombres
    '''

    n = len(tableau)
    
    for i in range(n):
        element_a_inserer = tableau[i]
        
        j = i
        while j > 0 and tableau[j - 1] > element_a_inserer:
            tableau[j] = tableau[j - 1]
            j = j - 1
        
        tableau[j] = element_a_inserer
        
    return tableau

def recherche_dichotomique(liste, element_recherche):
    '''
    Recherche la position d'un élément dans une liste triée.

    @param_entree : liste (triée)
    @param_entree : element_recherche
    '''

    n = len(liste)

    debut_sous_liste = 0
    fin_sous_liste = n - 1
    position = None
    trouve = False

    while trouve == False and debut_sous_liste <= fin_sous_liste:
        i_milieu = (debut_sous_liste + fin_sous_liste) // 2
        
        if liste[i_milieu] < element_recherche:
            debut_sous_liste = i_milieu + 1
        elif liste[i_milieu] > element_recherche:
            fin_sous_liste = i_milieu - 1
        elif liste[i_milieu] == element_recherche:
            trouve = True
            position = i_milieu

    return position

def _recherche_plus_petit(tableau, indice_depart):
    n = len(tableau)

    position_plus_petit = indice_depart
    for j in range(indice_depart, n):
        if tableau[j] < tableau[position_plus_petit]:
            position_plus_petit = j
            
    return position_plus_petit

def _echange_deux_elements(tableau, position_1, position_2):
    temp = tableau[position_1]
    tableau[position_1] = tableau[position_2]
    tableau[position_2] = temp
    
    return tableau

def tri_selection_factorise(tableau):
    '''
    Tri un tableau par la méthode de sélection.
    
    @param_entree : tableau (non_trié), liste de nombres
    @param_sortie : tableau (trié), liste de nombres
    '''
    
    n = len(tableau)

    for i in range(n):
        position_plus_petit = recherche_plus_petit(tableau, i)       
        echange_deux_elements(tableau, i, position_plus_petit)
        
    return tableau

## Fonction principale

def main():
    # resultat = somme(-3)
    # print(resultat)

    # print(somme(3))

    # resultat = factorielle(3)
    # print(resultat)

    # resultat = multiplication(3, 5)
    # print(resultat)

    # resultat = division(20, 6)
    # print(resultat)

    # resultat = est_pair(10)
    # print(resultat)
    # 
    # resultat = est_impair(10)
    # print(resultat)

    # resultat = est_premier(3)
    # print(resultat)
    # 
    # resultat = est_premier_moins_naif(3)
    # print(resultat)

    # resultat = pgcd(10, 15)
    # print(resultat)

    # resultat = somme_tableau([5, 9, 4, 3, 7, 9])
    # print(resultat)

    # resultat = moyenne_tableau([5, 9, 4, 3, 7, 9])
    # print(resultat)

    # resultat = recherche_minimum([5, 9, 4, 3, 7, 9])
    # print(resultat)

    # resultat = recherche_maximum([5, 9, 4, 3, 7, 9])
    # print(resultat)

    # resultat = recherche_premiere_occurence([5, 9, 4, 3, 7, 9], 3)
    # print(resultat)

#     resultat = recherche_toutes_occurences([5, 9, 4, 3, 7, 9], 9)
#     print(resultat)
    
#     resultat = insertion([5, 9, 4, 3, 7, 9], 10, 6)
#     print(resultat)
    
#     resultat = tri_selection([5, 9, 4, 3, 7, 9])
#     print(resultat)

#     resultat = tri_insertion([5, 9, 4, 3, 7, 9])
#     print(resultat)
    
#     resultat = recherche_dichotomique([1, 2, 3, 7, 9, 12, 15, 35, 42, 67, 99], 15)
#     print(resultat)

    resultat = tri_selection_factorise([5, 9, 4, 3, 7, 9])
    print(resultat)
    
## Programme principal
    
if __name__ == '__main__':
    main()