## Importation des modules

import algos_a_connaitre
import time
import random


## Déclaration des fonctions

def genere_tableau(n):
    '''
    Construit un tableau de n nombres entiers aléatoires entre 0 et 100.
    
    @param_entree : n, nombre entier
    @param_sortie : tableau, liste de nombres entiers
    '''
    
#     tableau = []
#     
#     for i in range(n):
#         tableau.append(random.randint(0, 100))

    # génération du même tableau par compréhension
    tableau = [random.randint(0, 100) for i in range(n)]

    return tableau


## Programme principal

# To do :
# importer le module
# exécuter la fonction somme_tableau sur un tableau à 10 éléments
# chronométrer son exécution avec perf_counter du module time

tableau = genere_tableau(10)
print(tableau)

# tableau.sort(reverse=True)
# 
# t1 = time.perf_counter()
# algos_a_connaitre.tri_selection(tableau)
# t2 = time.perf_counter()
# 
# duree = t2 - t1
# print("Durée d'exécution :", duree, 's')