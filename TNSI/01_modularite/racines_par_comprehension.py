## Importation des modules

import math


## Déclaration des fonctions


## Programme principal

liste_racines = [math.sqrt(i) for i in range(11)]
print(liste_racines)

tuple_racines = (math.sqrt(i) for i in range(11))
print(tuple(tuple_racines))

dico_racines = {i:math.sqrt(i) for i in range(11)}
print(dico_racines)