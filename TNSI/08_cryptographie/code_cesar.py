## Importation des modules


## Déclaration des fonctions

def lettre_decalee(lettre, decalage):
    alphabet = 'abcdefghijklmnopqrstuvwxyz '
    
    for i in range(len(alphabet)):
        if alphabet[i] == lettre:
            position = i
    
    position_nouvelle_lettre = (position + decalage) % len(alphabet)
    
    return alphabet[position_nouvelle_lettre]

def chiffre(message_clair, decalage):
    message_chiffre = ''
    
    for i in range(len(message_clair)):
        message_chiffre = message_chiffre + lettre_decalee(message_clair[i], decalage)
    
    return message_chiffre

def dechiffre(message_chiffre, decalage):
    message_clair = ''
    
    for i in range(len(message_chiffre)):
        message_clair = message_clair + lettre_decalee(message_chiffre[i], -1 * decalage)
    
    return message_clair


## Programme principal

message_clair = 'vive la nsi'
decalage = 3

message_chiffre = chiffre(message_clair, decalage)
print(message_chiffre)

message_clair_2 = dechiffre(message_chiffre, decalage)
print(message_clair_2)