#!/usr/bin/env python3
# -*- coding: utf-8 -*-


## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

class Liste_chainee:
    def __init__(self):
        self.tete = None
        
    def est_vide(self):
        if self.tete == None:
            return True
        else:
            return False

    def ajouter_element_queue(self, valeur):
        if self.est_vide() == True:
            self.tete = Maillon(valeur)
        else:
            maillon = self.tete
            while maillon.suivant != None:
                maillon = maillon.suivant
            
            maillon.suivant = Maillon(valeur)

    def liste(self):
        if self.est_vide() == True:
            return ()
        else:
            maillon = self.tete
            valeurs = (maillon.valeur,)
            while maillon.suivant != None:
                maillon = maillon.suivant
                valeurs = valeurs + (maillon.valeur,)
            
            return valeurs

    def longueur(self):
        if self.est_vide() == True:
            return 0
        else:
            maillon = self.tete
            compteur = 1
            while maillon.suivant != None:
                maillon = maillon.suivant
                compteur = compteur + 1
            
            return compteur

    def extrait_element(self, position):
        assert self.est_vide() == False, "La liste est vide"
        assert position < self.longueur(), "Indice hors de portée de la liste"
        assert position >= 0, "Indice hors de portée de la liste"
        
        maillon = self.tete
        compteur = 0
        while compteur != position:
            maillon = maillon.suivant
            compteur = compteur + 1
            
        return maillon.valeur

    def ajouter_element_tete(self, valeur):
        maillon = Maillon(valeur)
        maillon.suivant = self.tete
        self.tete = maillon

    def supprimer_element_queue(self):
        assert self.est_vide() == False, "La liste est vide"

        if self.longueur() == 1:
            self.tete = None
        else:
            maillon = self.tete
            while maillon.suivant != None:
                maillon_precedent = maillon
                maillon = maillon.suivant
            
            maillon_precedent.suivant = None
            del maillon

    def supprimer_element_tete(self):
        assert self.est_vide() == False, "La liste est vide"

        self.tete = self.tete.suivant


## Déclaration des fonctions


## Programme principal
