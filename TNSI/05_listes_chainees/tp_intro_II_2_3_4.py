## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant


## Déclaration des fonctions


## Programme principal

m1 = Maillon(5, None)
m2 = Maillon(7, None)

print(m1)
print(m1.valeur)
print(m1.suivant)

print(m2)
print(m2.valeur)
print(m2.suivant)