## Importation des modules


## Déclaration des classes


## Déclaration des fonctions


## Programme principal

t1 = [3, None]
t2 = [6, [7, None]]

t2[1][0] = 1
t1[1] = t2

t2[1][1] = [5, None]
t2[1][1][1] = [11, None]

print(id(t1), t1)
print(id(t1[1]), t1[1])
print(id(t1[1][1]), t1[1][1])
print(id(t1[1][1][1]), t1[1][1][1])
print(id(t1[1][1][1][1]), t1[1][1][1][1])