## Importation des modules


## Déclaration des classes


## Déclaration des fonctions


## Programme principal

t1 = [3, None]
t2 = [6, [7, None]]

print(t1)
print(t2)

t2[1][0] = 1

print(t1)
print(t2)

t1[1] = t2

print(t1)

print(id(t1))
print(id(t2))
print(id(t1[1]))