## Importation des modules

import TP_piles_liste_chainee


## Déclaration des classes

class File:
    def __init__(self):
        self.pile_1 = TP_piles_liste_chainee.Pile()
        self.pile_2 = TP_piles_liste_chainee.Pile()

    def est_vide(self):
        return self.pile_1.est_vide() and self.pile_2.est_vide()

    def enfiler(self, element):
        while self.pile_2.est_vide() == False:
            valeur = self.pile_2.depiler()
            self.pile_1.empiler(valeur)

        self.pile_2.empiler(element)

    def defiler(self):
        while self.pile_1.est_vide() == False:
            valeur = self.pile_1.depiler()
            self.pile_2.empiler(valeur)
        
        valeur = self.pile_2.depiler()
        
        return valeur

    def longueur(self):
        return self.pile_1.longueur() + self.pile_2.longueur()
    
    def tuple(self):
#         return self.pile_1.tuple() + self.pile_2.tuple()

        while self.pile_2.est_vide() == False:
            valeur = self.pile_2.depiler()
            self.pile_1.empiler(valeur)
        
        return self.pile_1.tuple()
    


## Déclaration des fonctions


## Fonction principale
    
def main():
    file_1 = File()
#     print(file_1.pile_1)
#     print(file_1.pile_2)
    
#     print(file_1.est_vide())
    
    file_1.enfiler(5)
#     print(file_1.est_vide())
#     print(file_1.pile_1.tuple())
#     print(file_1.pile_2.tuple())

    file_1.enfiler(7)
    file_1.enfiler(11)
    file_1.enfiler(14)
    file_1.enfiler(9)
#     print(file_1.pile_1.tuple())
#     print(file_1.pile_2.tuple())
    
#     print(file_1.defiler())
#     print(file_1.pile_1.tuple())
#     print(file_1.pile_2.tuple())
    
#     print(file_1.longueur())
#     print(file_1.defiler())
#     print(file_1.longueur())

    print(file_1.tuple())

## Programme principal

if __name__ == '__main__':
    main()
