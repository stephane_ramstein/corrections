## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant


class Liste_chainee:
    def __init__(self):
        self.tete = None

    def est_vide(self):
#         if self.tete == None:
#             return True
#         else:
#             return False
        
        return self.tete == None

    def ajouter_element_queue(self, valeur):
#         if self.est_vide() == True:
        if self.est_vide():
            self.tete = Maillon(valeur, None)
        else:
            maillon = self.tete
            
            while maillon.suivant != None:
                maillon = maillon.suivant
                
            maillon.suivant = Maillon(valeur, None)
            
    def liste(self):
        liste_valeurs = []
        
        if self.est_vide():
            return liste_valeurs
        else:
            maillon = self.tete
            liste_valeurs.append(maillon.valeur)            
            
            while maillon.suivant != None:
                maillon = maillon.suivant
                liste_valeurs.append(maillon.valeur)
            
            return liste_valeurs

    def longueur(self):
        if self.est_vide():
            return 0
        else:
            maillon = self.tete
            compteur = 1          
            
            while maillon.suivant != None:
                maillon = maillon.suivant
                compteur = compteur + 1
            
            return compteur

    def extrait_element(self, position):
        assert self.est_vide() == False, "La liste est vide"
        assert position >= 0 and position < self.longueur(), "Indice hors de portée"
        
        maillon = self.tete
        compteur = 0
        
        while compteur != position :
            maillon = maillon.suivant
            compteur = compteur + 1
        
        return maillon.valeur
    
    def ajouter_element_tete(self, valeur):
        if self.est_vide():
            self.tete = Maillon(valeur, None)
        else:
            nouveau_maillon = Maillon(valeur, None)
            nouveau_maillon.suivant = self.tete
            self.tete = nouveau_maillon

    def supprimer_element_queue(self):
        if self.est_vide():
            pass
        elif self.tete.suivant == None:
            self.tete = None
        else:
            maillon = self.tete
            
            while maillon.suivant.suivant != None:
                maillon = maillon.suivant
                
            del maillon.suivant
            
            maillon.suivant = None

    def supprimer_element_tete(self):
        if self.est_vide():
            pass
        elif self.tete.suivant == None:
            self.tete = None
        else:
            maillon_a_supprimer = self.tete
            self.tete = self.tete.suivant
            del maillon_a_supprimer

    def inserer_element(self, valeur, position):
        assert position >= 0 and position <= self.longueur(), "Indice hors de portée"
        
        if position == 0:
            self.ajouter_element_tete(valeur)
        else:
            maillon = self.tete
            compteur = 0
            
            while compteur != position - 1 :
                maillon = maillon.suivant
                compteur = compteur + 1        

            nouveau_maillon = Maillon(valeur, None)
            nouveau_maillon.suivant = maillon.suivant
            maillon.suivant = nouveau_maillon

    def supprimer_element(self, position):
        # Mettre les assert
        
        if position == 0:
            self.supprimer_element_tete()
        else:
            maillon = self.tete
            compteur = 0
            
            while compteur != position - 1:
                maillon = maillon.suivant
                compteur = compteur + 1   

            maillon_a_supprimer = maillon.suivant
            maillon.suivant = maillon.suivant.suivant
            del maillon_a_supprimer

    def chercher_element(self, valeur_recherchee):
        assert self.est_vide() == False, "La liste est vide"
        
        liste_positions = []
        maillon = self.tete
        compteur = 0

        if maillon.valeur == valeur_recherchee:
            liste_positions.append(0)

        while maillon.suivant != None:
            maillon = maillon.suivant
            compteur = compteur + 1
            if maillon.valeur == valeur_recherchee:
                liste_positions.append(compteur) 
                       
        return liste_positions
    
    def renverser(self):
        assert self.est_vide() == False, "La liste est vide"
        
        nouvelle_liste = Liste_chainee()
        
        maillon = self.tete
        nouvelle_liste.ajouter_element_tete(maillon.valeur)
        
        while maillon.suivant != None:
            maillon = maillon.suivant
            nouvelle_liste.ajouter_element_tete(maillon.valeur)
    
        return nouvelle_liste

    def modifier_valeur(self, nouvelle_valeur, position):
        assert self.est_vide() == False, "La liste est vide"
        assert position >= 0 and position < self.longueur(), "Indice hors de portée"
        
        maillon = self.tete
        compteur = 0
        
        while compteur != position :
            maillon = maillon.suivant
            compteur = compteur + 1
        
        maillon.valeur = nouvelle_valeur

    def permuter_elements(self, position_1, position_2):
        assert self.longueur() >= 2, 'La liste doit être au moins de longueur 2'
        assert position_1 >= 0 and position_1 < self.longueur(), 'index 1 out of range'
        assert position_2 >= 0 and position_2 < self.longueur(), 'index 2 out of range'
        
        valeur_1 = self.extrait_element(position_1)
        valeur_2 = self.extrait_element(position_2)
        
        self.modifier_valeur(valeur_2, position_1)
        self.modifier_valeur(valeur_1, position_2)
        

## Déclaration des fonctions


## Programme principal

l1 = Liste_chainee()
l1.ajouter_element_queue(5)
l1.ajouter_element_queue(7)
l1.ajouter_element_queue(11)
l1.ajouter_element_queue(4)
l1.ajouter_element_queue(11)
print(l1.liste())

# l1.ajouter_element_tete(9)
# print(l1.liste())
# 
# l1.supprimer_element_queue()
# print(l1.liste())
# 
# l1.supprimer_element_tete()
# print(l1.liste())

# l1.inserer_element(9, 4)
# print(l1.liste())

# l1.supprimer_element(3)
# print(l1.liste())

# print(l1.chercher_element(11))

# l2 = l1.renverser()
# print(l2.liste())

# l1.modifier_valeur(9, 2)
# print(l1.liste())

l1.permuter_elements(1, 2)
print(l1.liste())