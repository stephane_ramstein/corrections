## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant


class Liste_chainee:
    def __init__(self):
        self.tete = None

    def est_vide(self):
#         if self.tete == None:
#             return True
#         else:
#             return False
        
        return self.tete == None

    def ajouter_element_queue(self, valeur):
#         if self.est_vide() == True:
        if self.est_vide():
            self.tete = Maillon(valeur, None)
        else:
            maillon = self.tete
            
            while maillon.suivant != None:
                maillon = maillon.suivant
                
            maillon.suivant = Maillon(valeur, None)
            
    def liste(self):
        liste_valeurs = []
        
        if self.est_vide():
            return liste_valeurs
        else:
            maillon = self.tete
            liste_valeurs.append(maillon.valeur)            
            
            while maillon.suivant != None:
                maillon = maillon.suivant
                liste_valeurs.append(maillon.valeur)
            
            return liste_valeurs

    def longueur(self):
        if self.est_vide():
            return 0
        else:
            maillon = self.tete
            compteur = 1          
            
            while maillon.suivant != None:
                maillon = maillon.suivant
                compteur = compteur + 1
            
            return compteur


## Déclaration des fonctions


## Programme principal

l1 = Liste_chainee()
l1.ajouter_element_queue(5)
l1.ajouter_element_queue(7)
l1.ajouter_element_queue(11)
l1.ajouter_element_queue(4)

print(l1.liste())
print(l1.longueur())