## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant


## Déclaration des fonctions


## Programme principal

m1 = Maillon(5, None)
m2 = Maillon(7, None)

l1 = m1
l1.suivant = m2
l1.suivant.suivant = Maillon(2, None)
l1.suivant.suivant.suivant = Maillon(11, None)

l2 = Maillon(1, Maillon(2, Maillon(3, Maillon(4, None))))

print(l2.valeur)
print(l2.suivant.valeur)
print(l2.suivant.suivant.valeur)
print(l2.suivant.suivant.suivant.valeur)
print(l2.suivant.suivant.suivant.suivant)
print()

nouveau_maillon = Maillon(9, None)
nouveau_maillon.suivant = l2.suivant.suivant.suivant
l2.suivant.suivant.suivant = nouveau_maillon

print(l2.valeur)
print(l2.suivant.valeur)
print(l2.suivant.suivant.valeur)
print(l2.suivant.suivant.suivant.valeur)
print(l2.suivant.suivant.suivant.suivant.valeur)
print(l2.suivant.suivant.suivant.suivant.suivant)
print()