## Importation des modules


## Déclaration des classes

class Pile:
    def __init__(self):
        self.tableau = ()

    def est_vide(self):
        return self.tableau == ()

    def empiler(self, valeur):
        self.tableau = (valeur,) + self.tableau
        
    def depiler(self):
        assert self.est_vide() == False, 'La pile est vide'
        
        valeur_depilee = self.tableau[0]
        self.tableau = self.tableau[1:]
        
        return valeur_depilee
    
    def sommet(self):
        return self.tableau[0]
    
    def longueur(self):
#         return len(self.tableau)

        pile_temp = Pile()
        
        compteur = 0        
        while not(self.est_vide()):
#             valeur = self.depiler()
#             pile_temp.empiler(valeur)
            pile_temp.empiler(self.depiler())
            compteur = compteur + 1
            
        while not(pile_temp.est_vide()):
#             valeur = pile_temp.depiler()
#             self.empiler(valeur)
            self.empiler(pile_temp.depiler())
            
        return compteur
    
    def tuple(self):
        return self.tableau


## Déclaration des fonctions


## Fonction principale
    
def main():
    p1 = Pile()
    print(p1.est_vide())
    
    p1.empiler(5)
    print(p1.est_vide())
    
    p1.empiler(7)
    p1.empiler(11)
    p1.empiler(4)
    
#     print(p1.tableau)
    
#     print(p1.depiler())
#     print(p1.tableau)
#     
#     p1.depiler()
#     p1.depiler()
#     p1.depiler()
#     p1.depiler()

#     print(p1.sommet())
#     print(p1.longueur())

    print(p1.tuple())
    


## Programme principal

if __name__ == '__main__':
    main()
