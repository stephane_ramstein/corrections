## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant


## Déclaration des fonctions


## Programme principal

m1 = Maillon(5, None)
m2 = Maillon(7, None)

print(m2.valeur)
m2.valeur = 9
print(m2.valeur)