## Importation des modules

import module_liste_chainee


## Déclaration des classes

class File:
    def __init__(self, iterable=[]):
        self.liste_chainee = module_liste_chainee.Liste_chainee()
        
        for element in iterable:
            self.enfiler(element)

    def est_vide(self):
        return self.liste_chainee.est_vide()
    
    def enfiler(self, valeur):
        self.liste_chainee.ajouter_element_queue(valeur)
     
    def defiler(self):
        assert self.est_vide() == False, 'La file est vide'
        
        valeur = self.liste_chainee.extrait_element(0)
        self.liste_chainee.supprimer_element_tete()
        
        return valeur

    def longueur(self):
        return self.liste_chainee.longueur()
    
    def tuple(self):
        return self.liste_chainee.liste()


## Déclaration des fonctions


## Fonction principale
    
def main():
    p1 = File()
    print(p1)
    print(p1.est_vide())
   
    p1.enfiler(5)
    print(p1.est_vide())
    
    p1.enfiler(7)
    p1.enfiler(11)
    p1.enfiler(4)
    
    print(p1.tuple())
    
    print(p1.defiler())
    print(p1.tuple())

    print(p1.longueur())

    p2 = File('Informatique')
    print(p2.tuple())


## Programme principal

if __name__ == '__main__':
    main()
