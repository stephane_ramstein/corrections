## Importation des modules


## Déclaration des classes

class Maillon:
    def __init__(self, valeur=None, suivant=None):
        self.valeur = valeur
        self.suivant = suivant


## Déclaration des fonctions


## Programme principal

m1 = Maillon(5, None)
m2 = Maillon(7, None)

l1 = m1
l1.suivant = m2

print(l1.valeur)
print(l1.suivant.valeur)
print(l1.suivant.suivant)
print()

l1.suivant.suivant = Maillon(2, None)
print(l1.valeur)
print(l1.suivant.valeur)
print(l1.suivant.suivant.valeur)
print(l1.suivant.suivant.suivant)
print()

l1.suivant.suivant.suivant = Maillon(11, None)
print(l1.valeur)
print(l1.suivant.valeur)
print(l1.suivant.suivant.valeur)
print(l1.suivant.suivant.suivant.valeur)
print(l1.suivant.suivant.suivant.suivant)