## Importation des modules

import module_liste_chainee


## Déclaration des classes

class Pile:
    def __init__(self, iterable=[]):
        self.liste_chainee = module_liste_chainee.Liste_chainee()
        
        for element in iterable:
            self.empiler(element)

    def est_vide(self):
        return self.liste_chainee.est_vide()
    
    def empiler(self, valeur):
        self.liste_chainee.ajouter_element_queue(valeur)
     
    def depiler(self):
        assert self.est_vide() == False, 'La pile est vide'
        
        valeur = self.liste_chainee.extrait_element(self.liste_chainee.longueur() - 1)
        self.liste_chainee.supprimer_element_queue()
        
        return valeur

    def sommet(self):
#         valeur = self.liste_chainee.extrait_element(self.liste_chainee.longueur() - 1)
#         
#         return valeur
    
        return self.liste_chainee.extrait_element(self.liste_chainee.longueur() - 1)
   
    def longueur(self):
        return self.liste_chainee.longueur()
    
    def tuple(self):
        return self.liste_chainee.liste()


## Déclaration des fonctions


## Fonction principale
    
def main():
    p1 = Pile()
    print(p1)
    print(p1.est_vide())
#     
#     p1.empiler(5)
#     print(p1.est_vide())
#     
#     p1.empiler(7)
#     p1.empiler(11)
#     p1.empiler(4)
#     
#     print(p1.liste_chainee.liste())
#     
#     print(p1.depiler())
#     print(p1.liste_chainee.liste())
#     
# #     p1.depiler()
# #     p1.depiler()
# #     p1.depiler()
# #     p1.depiler()
# 
#     print(p1.sommet())
#     print(p1.longueur())
# 
#     print(p1.tuple())

    p2 = Pile('Informatique')
    print(p2.tuple())
    


## Programme principal

if __name__ == '__main__':
    main()
