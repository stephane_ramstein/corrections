## Importation des modules

import time


## Déclaration des fonctions

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

def fibonacci_top_down(n):
    if n in dico_solutions_rencontrees:
        solution = dico_solutions_rencontrees[n]
    else:   
        if n == 0:
            solution = 0
        elif n == 1:
            solution = 1
        else:
            solution = fibonacci_top_down(n - 1) + fibonacci_top_down(n - 2)
            
        dico_solutions_rencontrees[n] = solution

    return solution

def fibonacci_bottom_up(n):
    for i in range(n + 1):
        if i in dico_solutions_rencontrees:
            solution = dico_solutions_rencontrees[i]
        else:   
            if i == 0:
                solution = 0
            elif i == 1:
                solution = 1
            else:
                solution = dico_solutions_rencontrees[i - 1] + dico_solutions_rencontrees[i - 2]
            
        dico_solutions_rencontrees[i] = solution

    return solution

# Légèrement optimisé
def fibonacci_bottom_up(n):
    dico_solutions_rencontrees[0] = 0
    dico_solutions_rencontrees[1] = 1

    if n == 0:
        solution = dico_solutions_rencontrees[0]
    elif n == 1:
        solution = dico_solutions_rencontrees[1]
    else:
        for i in range(2, n + 1):
            solution = dico_solutions_rencontrees[i - 1] + dico_solutions_rencontrees[i - 2]
    
            dico_solutions_rencontrees[i] = solution

    return solution

## Programme principal

n = 35

t1 = time.perf_counter()
resultat = fibonacci(n)
t2 = time.perf_counter()
print("Temps mis par l'algorithme classique :", t2 - t1, 's.')

dico_solutions_rencontrees = {}
t1 = time.perf_counter()
resultat = fibonacci_top_down(n)
t2 = time.perf_counter()
print("Temps mis par l'algorithme dynamique top-down :", t2 - t1, 's.')

dico_solutions_rencontrees = {}
t1 = time.perf_counter()
resultat = fibonacci_bottom_up(n)
t2 = time.perf_counter()
print("Temps mis par l'algorithme dynamique bottom-up :", t2 - t1, 's.')