## Importation des modules

import time
import random


## Déclaration des fonctions

def recherche_lineaire(tableau, element):
    '''
    Retourne la position de la première occurence de l'élément dans le tableau.
    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''

    n = len(tableau)
    position = None
    
    i = 0
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
#             i += 1

    return position

def recherche_dichotomique(tableau, element_cherche):
    n = len(tableau)
    
    if n == 1:
        if element_cherche == tableau[0]:
            return 0
        else:
            return None
    else:
        i_milieu = n // 2
        
        if element_cherche >= tableau[i_milieu]:
            i_sous_liste = recherche_dichotomique(tableau[i_milieu:], element_cherche)
            if i_sous_liste != None:
                return i_milieu + i_sous_liste
            else:
                return None
        else:
            i_sous_liste = recherche_dichotomique(tableau[:i_milieu], element_cherche)
            if i_sous_liste != None:
                return 0 + i_sous_liste
            else:
                return None

def fusion(liste_1, liste_2):
    liste_1 = liste_1.copy()
    liste_2 = liste_2.copy()
    
    liste = []
    while liste_1 != [] and liste_2 != []:
        if liste_1[0] <= liste_2[0]:
#             liste.append(liste_1[0])
#             liste_1.pop(0)
            liste.append(liste_1.pop(0))
        else:
            liste.append(liste_2.pop(0))
    
    if liste_1 != []:
        liste = liste + liste_1
    else:
        liste = liste + liste_2
    
    return liste

def tri_fusion(liste):
    n = len(liste)
    
    if n == 1:
        return liste
    else:
        i_milieu = n // 2
        
        liste_1 = tri_fusion(liste[:i_milieu])
        liste_2 = tri_fusion(liste[i_milieu:])
        
        return fusion(liste_1, liste_2)
    
#         return fusion(tri_fusion(liste[:n // 2]), tri_fusion(liste[n // 2:]))

def tri_selection(tableau):
    '''
    Tri un tableau par la méthode de sélection.
    
    @param_entree : tableau (non_trié), liste de nombres
    @param_sortie : tableau (trié), liste de nombres
    '''
    
    n = len(tableau)

    for i in range(n):
        position_plus_petit = i
        for j in range(i, n):
            if tableau[j] < tableau[position_plus_petit]:
                position_plus_petit = j
        
        temp = tableau[i]
        tableau[i] = tableau[position_plus_petit]
        tableau[position_plus_petit] = temp
        
    return tableau

def fusion_recursive(liste_1, liste_2):
    liste_1 = liste_1.copy()
    liste_2 = liste_2.copy()
    
    if liste_1 == []:
        return liste_2
    elif liste_2 == []:
        return liste_1
    else:
        if liste_1[0] <= liste_2[0]:
            plus_petit = liste_1.pop(0)
            return [plus_petit] + fusion_recursive(liste_1, liste_2)
        else:
            plus_petit = liste_2.pop(0)
            return [plus_petit] + fusion_recursive(liste_1, liste_2)


## Programme principal
liste_1 = [74, 15, 53, 27, 20]
liste_2 = [97, 87, 68, 42, 60]
liste_1.sort()
liste_2.sort()

liste = fusion(liste_1, liste_2)
print(liste)

liste = fusion_recursive(liste_1, liste_2)
print(liste)
