## Importation des modules

import time


## Déclaration des fonctions

def indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire):
    i = 0
    while i < len(systeme_monetaire) and systeme_monetaire[i] <= valeur:
        i = i + 1
    
    return i - 1

def rendu_glouton(valeur, systeme_monetaire):
    liste_pieces_a_rendre = []
    while valeur > 0:
        indice_piece = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
        liste_pieces_a_rendre.append(systeme_monetaire[indice_piece])
        valeur = valeur - systeme_monetaire[indice_piece]
        
    return liste_pieces_a_rendre

def rendu_glouton_rec(valeur, systeme_monetaire):
    if valeur <= 0:
        return []
    else:
        indice_piece = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
        valeur_restante = valeur - systeme_monetaire[indice_piece]

        return [systeme_monetaire[indice_piece]] + rendu_glouton_rec(valeur_restante, systeme_monetaire)

# def rendu_recursif_top_down(valeur, systeme_monetaire):
#     if valeur <= 0:
#         return []
#     else:
#         indice_piece = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
#         liste_solutions = []
#         for i in range(indice_piece + 1):
#             resultat = rendu_recursif_top_down(valeur - systeme_monetaire[i], systeme_monetaire)
#             for el in resultat:
#                 print(el, systeme_monetaire[i])
#                 liste_solutions.append([el, systeme_monetaire[i]])
#         return liste_solutions

def rendu_recursif_top_down(valeur, systeme_monetaire):
    if valeur <= 0:
        return []
    else:
        indice_piece = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
        liste_solutions = []
        for i in range(indice_piece + 1):
            resultat = [systeme_monetaire[i]] + rendu_recursif_top_down(valeur - systeme_monetaire[i], systeme_monetaire)
            liste_solutions.append(resultat)
            
        return liste_solutions

## Programme principal

systeme_monetaire = [1, 2, 5, 10]
valeur = 30

t1 = time.perf_counter()
resultat = rendu_recursif_top_down(valeur, systeme_monetaire)
t2 = time.perf_counter()
print("temps mis par l'algorithme :", t2 - t1, 's.')
print(resultat)