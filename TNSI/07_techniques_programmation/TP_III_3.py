## Importation des modules

import time


## Déclaration des fonctions

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


## Programme principal

n = 35

t1 = time.perf_counter()
resultat = fibonacci(n)
t2 = time.perf_counter()
print(resultat)

print("Temps mis par l'algorithme :", t2 - t1, 's.')