## Importation des modules

import cv2
import numpy as np


## Déclaration des fonctions

def rotation(image):
    image = np.copy(image)
    
    if len(image) == 1:
        return image
    else:
        n = len(image)
        milieu = len(image) // 2
        
        # On coupe en 4
        sous_image_1 = image[0:milieu, 0:milieu]
        sous_image_2 = image[0:milieu, milieu:n]
        sous_image_3 = image[milieu:n, milieu:n]
        sous_image_4 = image[milieu:n, 0:milieu]
        
        # On tourne chaque sous image
        sous_image_1 = rotation(sous_image_1)
        sous_image_2 = rotation(sous_image_2)
        sous_image_3 = rotation(sous_image_3)
        sous_image_4 = rotation(sous_image_4)
        
        # On permute chaque sous image
        image[0:milieu, 0:milieu] = sous_image_4
        image[milieu:n, 0:milieu] = sous_image_3
        image[milieu:n, milieu:n] = sous_image_2
        image[0:milieu, milieu:n] = sous_image_1
        
        return image


## Programme principal

image = cv2.imread('manchot.png')
image_tournee = rotation(image)

cv2.imshow('mon image de manchot', image)
cv2.waitKey(0)

cv2.imshow('mon image de manchot', image_tournee)
cv2.waitKey(0)

cv2.destroyAllWindows()