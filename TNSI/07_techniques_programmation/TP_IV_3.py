## Importation des modules

import time


## Déclaration des fonctions

def indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire):
    i = 0
    while i < len(systeme_monetaire) and systeme_monetaire[i] <= valeur:
        i = i + 1
    
    return i - 1


## Programme principal

systeme_monetaire = [1, 2, 5, 10]
valeur = 14

resultat = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
print(resultat)