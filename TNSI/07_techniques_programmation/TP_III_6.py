## Importation des modules

import time


## Déclaration des fonctions

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

def fibonacci_top_down(n):
    if n in dico_solutions_rencontrees:
        solution = dico_solutions_rencontrees[n]
    else:   
        if n == 0:
            solution = 0
        elif n == 1:
            solution = 1
        else:
            solution = fibonacci_top_down(n - 1) + fibonacci_top_down(n - 2)
            
        dico_solutions_rencontrees[n] = solution

    return solution


## Programme principal

n = 15

# t1 = time.perf_counter()
# resultat = fibonacci(n)
# t2 = time.perf_counter()
# print(resultat)
# 
# print("Temps mis par l'algorithme :", t2 - t1, 's.')

dico_solutions_rencontrees = {}
resultat = fibonacci_top_down(n)
print(resultat)
print(dico_solutions_rencontrees)