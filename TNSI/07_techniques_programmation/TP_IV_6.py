## Importation des modules

import time


## Déclaration des fonctions

def indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire):
    i = 0
    while i < len(systeme_monetaire) and systeme_monetaire[i] <= valeur:
        i = i + 1
    
    return i - 1

def rendu_glouton(valeur, systeme_monetaire):
    liste_pieces_a_rendre = []
    while valeur > 0:
        indice_piece = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
        liste_pieces_a_rendre.append(systeme_monetaire[indice_piece])
        valeur = valeur - systeme_monetaire[indice_piece]
        
    return liste_pieces_a_rendre

def rendu_glouton_rec(valeur, systeme_monetaire):
    if valeur <= 0:
        return []
    else:
        indice_piece = indice_plus_grande_piece_a_rendre(valeur, systeme_monetaire)
        valeur_restante = valeur - systeme_monetaire[indice_piece]

        return [systeme_monetaire[indice_piece]] + rendu_glouton_rec(valeur_restante, systeme_monetaire)


## Programme principal

systeme_monetaire = [1, 2, 5, 10]
valeur = 14

resultat = rendu_glouton(valeur, systeme_monetaire)
print(resultat)

resultat = rendu_glouton_rec(valeur, systeme_monetaire)
print(resultat)

systeme_monetaire = [1, 7, 9]
valeur = 14

resultat = rendu_glouton(valeur, systeme_monetaire)
print(resultat)

resultat = rendu_glouton_rec(valeur, systeme_monetaire)
print(resultat)

systeme_monetaire = [2, 7, 9]
valeur = 14

resultat = rendu_glouton(valeur, systeme_monetaire)
print(resultat)

resultat = rendu_glouton_rec(valeur, systeme_monetaire)
print(resultat)