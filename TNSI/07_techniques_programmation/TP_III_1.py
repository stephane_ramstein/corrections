## Importation des modules


## Déclaration des fonctions

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


## Programme principal

n = 5

resultat = fibonacci(n)
print(resultat)