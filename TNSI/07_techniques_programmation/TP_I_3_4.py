## Importation des modules

import time
import random


## Déclaration des fonctions

def recherche_lineaire(tableau, element):
    '''
    Retourne la position de la première occurence de l'élément dans le tableau.
    @param_entree : tableau
    @param_entree : element
    @param_sortie : position
    '''

    n = len(tableau)
    position = None
    
    i = 0
    while i < n and position == None:
        if tableau[i] == element:
            position = i
        else:
            i = i + 1
#             i += 1

    return position

def recherche_dichotomique(tableau, element_cherche):
    n = len(tableau)
    
    if n == 1:
        if element_cherche == tableau[0]:
            return 0
        else:
            return None
    else:
        i_milieu = n // 2
        
        if element_cherche >= tableau[i_milieu]:
            i_sous_liste = recherche_dichotomique(tableau[i_milieu:], element_cherche)
            if i_sous_liste != None:
                return i_milieu + i_sous_liste
            else:
                return None
        else:
            i_sous_liste = recherche_dichotomique(tableau[:i_milieu], element_cherche)
            if i_sous_liste != None:
                return 0 + i_sous_liste
            else:
                return None


## Programme principal

# tableau = [random.randint(0, 100) for i in range(10)]
tableau = [74, 15, 53, 27, 20, 97, 87, 68, 42, 60]
tableau.sort()

print(tableau)

resultat = recherche_lineaire(tableau, 87)
print(resultat)


resultat = recherche_dichotomique(tableau, 87)
print(resultat)