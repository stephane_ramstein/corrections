## Importation des modules

import time


## Déclaration des fonctions

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

def fibonacci_top_down(n):
    if n in dico_solutions_rencontrees:
        solution = dico_solutions_rencontrees[n]
    else:   
        if n == 0:
            solution = 0
        elif n == 1:
            solution = 1
        else:
            solution = fibonacci_top_down(n - 1) + fibonacci_top_down(n - 2)
            
        dico_solutions_rencontrees[n] = solution

    return solution


## Programme principal

n = 35

t1 = time.perf_counter()
resultat = fibonacci(n)
t2 = time.perf_counter()
print("Temps mis par l'algorithme classique :", t2 - t1, 's.')

dico_solutions_rencontrees = {}
t1 = time.perf_counter()
resultat = fibonacci_top_down(n)
t2 = time.perf_counter()
print("Temps mis par l'algorithme dynamique :", t2 - t1, 's.')