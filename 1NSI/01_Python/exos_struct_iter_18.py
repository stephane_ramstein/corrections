## Importation des modules


## Déclaration des fonctions


## Programme principal

nombre = int(input('Entrer un nombre entier : '))

compteur = 0

while nombre > 0 and nombre % 2 == 0:
    compteur = compteur + 1
    nombre = nombre // 2

print('Ce nombre est divisible', compteur, 'fois par 2.')