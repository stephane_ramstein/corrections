## Importation des modules


## Déclaration des fonctions


## Programme principal

# voyelles = ('a', 'e', 'i', 'o', 'u', 'y')
# consonnes = ('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z')

# voyelles = ['a', 'e', 'i', 'o', 'u', 'y']
# consonnes = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z']

voyelles = 'aeiouy'
consonnes = 'bcdfghjklmnpqrstvwxz'

caractere = input('Entrer un caractère : ')

if caractere in voyelles:
    print("c'est une voyelle")
elif caractere in consonnes:
    print("c'est une consonne")
else:
    print("Le caractère n'est pas une lettre")