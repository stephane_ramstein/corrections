## Importation des modules


## Déclaration des fonctions


## Programme principal

voyelles = 'aeiouy'
consonnes = 'bcdfghjklmnpqrstvwxz'
chiffres = '0123456789'

caractere = input('Entrer un caractère : ')

if caractere in voyelles:
    print("c'est une voyelle")
elif caractere in consonnes:
    print("c'est une consonne")
elif caractere in chiffres:
    print("c'est un chiffre")
else:
    print("Le caractère n'est pas une lettre ni un chiffre")