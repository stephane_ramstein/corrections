## Importation des modules


## Déclaration des fonctions


## Programme principal

a = int(input('Entrer un nombre entier : '))

if a % 2 == 0:
    print('Le nombre est pair')
else:
    print("Le nombre est impair")


# Fonctionne aussi
#
# if a % 2 == 1:
#     print('Le nombre est impair')
# else:
#     print("Le nombre est pair")
# 
# 
# if a % 2 != 0:
#     print('Le nombre est impair')
# else:
#     print("Le nombre est pair")