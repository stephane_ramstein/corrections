## Importation des modules


## Déclaration des fonctions


## Programme principal

continuer = True

while continuer == True:
    entree_clavier = input('Entrer un prix HT en euros (FIN pour arrêter) : ')
    if entree_clavier != 'FIN':
        prix_HT = float(entree_clavier)
        taux_TVA = input('Quelle TVA appliquer ? (1 pour TVA à 5.5 % ou 2 pour TVA ) 20% : ')
        if taux_TVA == '1':
            prix_TTC = prix_HT * 1.055
            print('Le prix TTC est', prix_TTC, 'euros')
        elif taux_TVA == '2':
            prix_TTC = prix_HT * 1.2
            print('Le prix TTC est', prix_TTC, 'euros')
        else:
            print('Il fallait choisir entre 1 ou 2')
    else:
        continuer = False