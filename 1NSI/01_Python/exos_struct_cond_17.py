## Importation des modules


## Déclaration des fonctions


## Programme principal

# annee = int(input('Entrer une année : '))
# 
# if (annee % 4 == 0) and (annee % 100 != 0):
#     bissextile = True
# elif annee % 400 == 0:
#     bissextile = True
# else:
#     bissextile = False
#     
# print('Année bissextile :', bissextile)


# On peut réduire le code

annee = int(input('Entrer une année : '))

if ((annee % 4 == 0) and (annee % 100 != 0)) or (annee % 400 == 0):
    bissextile = True
else:
    bissextile = False
    
print('Année bissextile :', bissextile)
