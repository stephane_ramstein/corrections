etudiants = [
    {"nom": "Alice", "age": 20, "note": 14},
    {"nom": "Bob", "age": 22, "note": 16},
    {"nom": "Charlie", "age": 21, "note": 12},
    {"nom": "Alice", "age": 25, "note": 15}, 
    {"nom": "Lucas", "age": 22, "note": 18},  
    {"nom": "Emma", "age": 20, "note": 10},  
    {"nom": "Noah", "age": 23, "note": 14}, 
    {"nom": "Sophie", "age": 24, "note": 17},
]


## Pseudo-code à traduire en Python

# Fonction meilleur_etudiant(liste):
#     Initialiser n à la taille de la liste
#     Initialiser indice_max à 0
#     Pour i allant de 1 à n - 1:
#         Si la ligne_dico[i]['note'] est plus grand que etudiants[indice_max]['note']:
#             indice_max = i
#     Retourner etudiant[indice_max]
