## Importation des modules

## Définition des fonctions

def est_carre(m):
    """
    Renvoie True si la matrice m est carrée, False sinon
    
    @param m : list[list[]]
    @returns: bool
    """
    dimension = len(m)
    for ligne in m :
        if len(ligne) != dimension:
            return False
    return True

def est_magique(m):
    """
    Renvoie True si la matrice m est magique, False sinon
    
    @param m : list[list[int]]
    @returns: bool
    """
    somme_diag1 = 0
    somme_diag2 = 0
    if not est_carre(m):
        return False
    somme = 0
    for e in m[1]:
        somme += e
        
        
    for i in range(len(m)):
        somme_ligne = 0
        
        #Calcul la somme de chaque ligne
        for e in m[i]:
            somme_ligne += e
        if not somme_ligne == somme:
            return False
        
        #Calcul la somme de chaque colonne
        somme_colonne = 0
        for j in range(len(m)):
            somme_colonne += m[j][i]
        if not somme_colonne == somme:
            return False
            
        #Calcul des diagonales
        somme_diag1 += m[i][i]
        
        somme_diag2 += m[i][len(m)-1-i]

    if not somme_diag1 == somme:
        return False
    
    if not somme_diag2 == somme:
        return False
    return True
    

## Programme principal


M = [[2,7,6],
    [9,5,1],
    [4,3,8]]

print(M[1][1])

print(est_magique(M))