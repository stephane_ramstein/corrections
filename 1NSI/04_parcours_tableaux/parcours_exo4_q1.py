## Importation des modules


## Définition des fonctions

def compte_superieur_a(tab, seuil):
    """
    Renvoie le nombre d'éléments supérieur a seuil
    présents dans tab
    
    :param tab: list
    :param seuil: int
    :returns: int
    """
    cpt = 0
    for e in tab:
        if seuil < e:
            cpt += 1
    return cpt

## Programme principal

l = [4,5,6,8,3,12,9]

print(compte_superieur_a(l,4))