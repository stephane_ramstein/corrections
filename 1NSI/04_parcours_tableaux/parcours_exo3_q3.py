#Importation des modules

#Déclaration des fonctions

def moyenne_elements(tab):
    """
    Renvoie la moyenne des éléments du tableau
    
    @param tab : list
    @returns: float
    """
    cpt = 0
    for e in tab:
        cpt += e
    return cpt/len(tab)

#Programme principal

tab = [10, 20, 30]
print(moyenne_elements(tab))