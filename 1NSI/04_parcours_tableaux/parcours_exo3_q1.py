#Importation des modules

#Déclaration des fonctions

def somme_elements(tab):
    """
    Renvoie la somme de tous les éléments du tableau tab
    @param tab: list
    @returns: int
    """
    cpt = 0
    for e in tab:
        cpt += e
    return cpt

#Programme principal

tab = [4, 7, 1, 3]
print(somme_elements(tab))