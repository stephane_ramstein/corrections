#Importation des modules

#Déclaration des fonctions

def maximum(tab):
    """
    Renvoie le chiffre le plus grand présent dans tab.
    
    @param tab: list
    @returns int
    """
    assert tab != [] , "la liste est vide"
    maxi = tab[0]
    for e in tab:
        if e > maxi:
            maxi = e
    return maxi

#Programme principal

tab = [12, -3, 15, -9, 17, 7]
print(maximum(tab))
print(maximum([]))