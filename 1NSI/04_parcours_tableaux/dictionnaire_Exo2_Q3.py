## Importation des modules

## Définition des fonctions


def comptage(mot):
    """
    pour chaque caractère de mot associe le nombre d'occurencesde chaque lettre
    
    @param_entrée: mot, chaine de carctères, ex! "Hello World!"
    @param_sortie: dictionaire, ex: {"a": 1, "b": 3, "c": 99}
    """
    dictionaire = {}
    
    for i in range(len(mot)):
        if mot[i] in dictionaire:
            dictionaire[mot[i]] += 1
        else:
            dictionaire[mot[i]] = 1
    return dictionaire


## Programme principal

print(comptage("Hello World!"))