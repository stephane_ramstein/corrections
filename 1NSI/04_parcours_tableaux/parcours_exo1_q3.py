#Importation des modules

#Déclaration des fonctions

def recherche(tab, elt):
    """
    Recherche elt dans tab, renvoie True si elt est présent,
    False sinon.
    
    @param tab : list
    @param elt
    @returns : bool
    """
    for e in tab:
        if e == elt:
            return True
        else:
            return False
    
#Programme principal

tab = [12, -3, 15, -9, 17, 7]