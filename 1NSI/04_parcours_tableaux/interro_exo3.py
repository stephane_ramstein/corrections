## Importation des modules


## Définition des fonctions

def compte_superieur_a(tab, seuil):
    """
    Renvoie le nombre d'éléments supérieurs a seuil
    présents dans tab
    
    :param tab: list
    :param seuil: int
    :returns: int
    """
    cpt = 0
    for e in tab:
        if e > seuil:
            cpt += 1
    return cpt

## Programme principal

