## Importation des modules


## Définition des fonctions

def filtre_inferieur_a(tab, seuil):
    """
    Renvoie la liste des éléments inférieurs ou égaux a seuil
    présents dans tab
    
    :param tab: list
    :param seuil: int
    :returns: list
    """
    res = []
    for e in tab:
        if seuil >= e:
            res.append(e)
    return res

## Programme principal

l = [4,5,6,8,3,12,9]

print(filtre_inferieur_a(l,4))