## Importation des modules


## Définition des fonctions

def recherche(tab, elt):
    """Recherche si elt est présent dans tab"""
    for elem in tab:
        if elem == elt:
            return True
    return False

tab = [1, 2, 3]
elt = 2
print(recherche(tab, elt))

## Programme principal