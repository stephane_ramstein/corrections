#Importation des modules

#Déclaration des fonctions

def produit_elements(tab):
    """
    Renvoie le produit de tous les éléments du tableau tab
    @param tab: list
    @returns: int
    """
    cpt = 1
    for e in tab:
        cpt *= e
    return cpt

#Programme principal

tab = [1, 2, 3, 4]
print(produit_elements(tab))