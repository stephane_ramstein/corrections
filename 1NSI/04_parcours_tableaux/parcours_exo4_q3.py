## Importation des modules


## Définition des fonctions

def proportion_superieur_a(tab, seuil):
    """
    Renvoie le pourcentage d'éléments supérieur a seuil
    présents dans tab
    
    :param tab: list
    :param seuil: int
    :returns: float
    """
    if tab == []:
        return 0.0
    cpt = 0
    for e in tab:
        if seuil > e:
            cpt += 1
    return cpt/len(tab)*100

## Programme principal

l = [4,5,6,8,3,12,9]

print(proportion_superieur_a(l,4))