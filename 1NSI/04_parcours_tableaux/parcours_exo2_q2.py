#Importation des modules

#Déclaration des fonctions

def minimum(tab):
    """
    Renvoie le chiffre le plus petit présent dans tab.
    
    pré-condition : tab ne doit pas être vide
    @param tab: list
    @returns int
    """
    assert tab != [], "la liste est vide"
    mini = tab[0]
    for e in tab:
        if e < mini:
            mini = e
    return mini

#Programme principal

tab = [12, -3, 15, -9, 17, 7]
print(minimum(tab))
print(minimum([]))