# Importation des modules


# Déclaration des fonctions

def est_valide(mdp: str) -> bool:
    '''
    Teste si un mot de passe est valide.
    
    @param_entree : mdp, chaine de caractères
    @param_sortie : booléen
    '''
    
    majuscules = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    minuscules = "abcdefghijklmnopqrstuvwxyz"
    chiffres = "0123456789"
    caracteres_non_speciaux = majuscules + minuscules + chiffres
    
    condition_maj = False
    condition_min = False
    condition_chi = False
    condition_car = False
    
    for i in mdp:
        if i in majuscules:
            condition_maj = True
        if i in minuscules:
            condition_min = True
        if i in chiffres:
            condition_chi = True
        if i not in caracteres_non_speciaux:
            condition_car = True
    
    if (condition_maj == True and condition_min == True
       and condition_chi == True and condition_car == True):

        return True
    
    else:
        return False


def nb_mots(phrase: str) -> int:
    '''
    Retourne nombre de mots d'une phrase.
    Version sans utiliser la méthode split.
    
    @param_entree : phrase, chaine de caractères
    @param_sortie : cpt, nombre entier
    '''
    
    if phrase == "":
        cpt = 0
    else:
        cpt = 1
    for i in range(len(phrase)):
        if phrase[i] == " ":
            cpt = cpt + 1
            
    return cpt
    
def nb_mots2(phrase: str) -> int:
    '''
    Retourne nombre de mots d'une phrase.
    Version en utilisant la méthode split.
    
    @param_entree : phrase, chaine de caractères
    @param_sortie : cpt, nombre entier
    '''
    
    if phrase == "":
        return 0
    else:
        return len(phrase.split(" "))


## Programme principal

# print(est_valide("abc"))
# print(est_valide("B0njour!"))

print(nb_mots("Le petit chat est mort"))
print(nb_mots("Bonjour"))

print(nb_mots2("Le petit chat est mort"))
print(nb_mots2(""))
    
