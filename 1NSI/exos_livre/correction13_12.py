# Declaration des fonctions

def est_ordonne(tab):
    """
    Renvoie True si le tableau est dans l'ordre croissant, False sinon.
    
    @param tab : list
    """
    res = True
    for i in range(len(tab)):
        if i != 0:
            if tab[i] < tab[i-1]:
                res = False
    return res

def palindrome(chaine):
    """
    Verifie si chaine est un palindrome ou non,
    renvoie True si oui et False si non.
    
    @param chaine : str
    @returns : bool
    """
    res = True
    
    for i in range(len(chaine)):
        if chaine[i] != chaine[-i-1]:
            res = False

    return res

# programme principal

print(palindrome("MAMAN"))
print(palindrome("KAYAK"))






