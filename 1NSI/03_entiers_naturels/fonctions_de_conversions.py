## Importation des modules


## Définition des fonctions

def bin_to_dec(nombre_bin):
    '''
    Converti un nombre binaire en nombre décimal.
    
    @param_entree : nombre_bin, chaine de caractères, par exemple '00111010'
    @param_sortie : nombre_dec, nombre entier, par exemple 58
    '''
    
    nombre_dec = 0
    for i in range(len(nombre_bin)):
        if nombre_bin[len(nombre_bin) - 1 - i] == '1':
           nombre_dec = nombre_dec + 2 ** i
    
    return nombre_dec

def complete_8_bits(nombre_bin):
    '''
    Ajoute des 0 nons significatifs à gauche d'un bombre binaire pour qu'il comporte 8 bits.
    
    @param_entree : nombre_bin, chaine de caractères, par exemple '101'
    @param_sortie : nombre_bin_compl, chaine de caractères, par exemple '00000101'    
    '''
    
    n = len(nombre_bin)
    
    nombre_bin_compl = '0' * (8 - n) + nombre_bin
    
    return nombre_bin_compl

def dec_to_bin(nombre_dec):
    '''
    Converti un nombre décimal en nombre binaire.
    
    @param_entree : nombre_dec, nombre entier, par exemple 67
    @param_sortie : nombre_bin, chaine de caractères, par exemple '1000011'
    '''
    
    nombre_bin = ''
    quotient = None
    
    while quotient != 0:
        quotient = nombre_dec // 2
        reste = nombre_dec % 2
        nombre_bin = str(reste) + nombre_bin
        nombre_dec = quotient
    
    nombre_bin = complete_8_bits(nombre_bin)
    
    return nombre_bin  

def inv(nombre_bin):
    '''
    Inverse un nombre binaire.
    
    @param_entree : nombre_bin, chaine de caractères, par exemple '10111101'
    @param_sortie : nouveau_nombre_bin, chaine de caractères, par exemple '01000010'
    '''
    
    nouveau_nombre_bin = ""
    for bit in nombre_bin:
        if bit == "0":
            nouveau_nombre_bin += "1"
        else:
            nouveau_nombre_bin += "0"
    
    return nouveau_nombre_bin

# def add_1_bit(bit_a, bit_b, bit_r):
#     '''
#     Additionne deux bits avec retenue.
#     
#     Solution avec if, elif et else.
#     
#     @param_entree : bit_a, chaine de caractères, par exemple '1'
#     @param_entree : bit_b, chaine de caractères, par exemple '0'
#     @param_entree : bit_r, chaine de caractères, par exemple '1'
#     @param_sortie : bit_s, chaine de caractères, par exemple '0'
#     @param_sortie : nouveau_bit_r, chaine de caractères, par exemple '1'
#     '''
# 
#     # 
#     if bit_a == 0 and bit_b == 0 and bit_r = 0:
#         bit_s = 0
#         nouveau_bit_r = 0
#     elif bit_a == 0 and bit_b == 1 and bit_r = 0:
#         bit_s = 1
#         nouveau_bit_r = 0
#         
#     ...  # à compléter
#     
#     return bit_s, nouveau_bit_r

# def add_1_bit(bit_a, bit_b, bit_r):
#     '''
#     Additionne deux bits avec retenue.
#     
#     Solution avec deux tableaux stockant la table de vérité.
#     
#     @param_entree : bit_a, chaine de caractères, par exemple '1'
#     @param_entree : bit_b, chaine de caractères, par exemple '0'
#     @param_entree : bit_r, chaine de caractères, par exemple '1'
#     @param_sortie : bit_s, chaine de caractères, par exemple '0'
#     @param_sortie : nouveau_bit_r, chaine de caractères, par exemple '1'
#     '''
#   
#     entree = ((0, 0, 0), (0, 1, 0), (1, 0, 0), (1, 1, 0),
#               (0, 0, 1), (0, 1, 1), (1, 0, 1), (1, 1, 1))
#     sortie = ((0, 0), (1, 0), (1, 0), (0, 1),
#               (1, 0), (0, 1), (0, 1), (1, 1))
#     
#     position = None
#     for i in range(len(entree)):
#         if entree[i] == (bit_a, bit_b, bit_r):
#             position = i
#     
#     return sortie[position]

def add_1_bit(bit_a, bit_b, bit_r):
    '''
    Additionne deux bits avec retenue.
    
    Solution avec un tableau associatif, stockant la table de vérité,
    c'est-à-dire un dictionnaire.
    
    @param_entree : bit_a, chaine de caractères, par exemple '1'
    @param_entree : bit_b, chaine de caractères, par exemple '0'
    @param_entree : bit_r, chaine de caractères, par exemple '1'
    @param_sortie : bit_s, chaine de caractères, par exemple '0'
    @param_sortie : nouveau_bit_r, chaine de caractères, par exemple '1'
    '''
    
    table_de_verite = {('0', '0', '0'):('0', '0'), ('0', '1', '0'):('1', '0'), ('1', '0', '0'):('1', '0'), ('1', '1', '0'):('0', '1'),
          ('0', '0', '1'):('1', '0'), ('0', '1', '1'):('0', '1'), ('1', '0', '1'):('0', '1'), ('1', '1', '1'):('1', '1')}

    return table_de_verite[(bit_a, bit_b, bit_r)]

def add_8_bits(nombre_bin_a, nombre_bin_b):
    '''
    Additionne deux nombres binaires.
    
    @param_entree : nombre_bin_a, chaine de caractères, par exemple '10111101'
    @param_entree : nombre_bin_b, chaine de caractères, par exemple '00101011'
    @param_sortie : nombre_bin_c, chaine de caractères, par exemple '11101000'
    '''
    
    nombre_bin_c = ''
    retenue = '0'
    for i in range(7, -1, -1):
        bit_c, retenue = add_1_bit(nombre_bin_a[i], nombre_bin_b[i], retenue)
        nombre_bin_c = bit_c + nombre_bin_c
    
    return nombre_bin_c
  
def dec_to_bin_rel(nombre_dec):
    '''
    Converti un nombre décimal relatif en nombre binaire.
    
    @param_entree : nombre_dec, nombre entier négatif, par exemple -67
    @param_sortie : nombre_bin, chaine de caractères, par exemple '10111101'
    '''
    
    if nombre_dec >= 0:
        nombre_bin = dec_to_bin(nombre_dec)
    else:
        nombre_dec = abs(nombre_dec)
        nombre_bin = dec_to_bin(nombre_dec)
        nombre_bin = inv(nombre_bin)
        nombre_bin = add_8_bits(nombre_bin, '00000001')
 
    return nombre_bin

def bin_to_dec_rel(nombre_bin):
    '''
    Converti un nombre binaire relatif en nombre décimal.
    
    @param_entree : nombre_bin, chaine de caractères, par exemple '10111101'
    @param_sortie : nombre_dec, nombre entier négatif, par exemple -67
    '''
    
    if len(nombre_bin) < 8:
        nombre_bin = complete_8_bits(nombre_bin)
    
    if nombre_bin[0] == '0':
        nombre_dec = bin_to_dec(nombre_bin)
    else:
        nombre_bin = add_8_bits(nombre_bin, '11111111')
        nombre_bin = inv(nombre_bin)
        nombre_dec = bin_to_dec(nombre_bin)
        nombre_dec = -1 * nombre_dec

    return nombre_dec


## Programme principal

# resultat = bin_to_dec('00111010')
# print(resultat)
# 
# resultat = dec_to_bin(67)
# print(resultat)
# 
# resultat = inv('10111101')
# print(resultat)

# print(add_1_bit('1', '0', '1'))

# print(add_8_bits('10111101', '00101011'))

# print(complete_8_bits('101'))

# print(dec_to_bin_rel(67))
# print(dec_to_bin_rel(-67))

print(bin_to_dec_rel('10111101'))
print(bin_to_dec_rel('01000011'))
print(bin_to_dec_rel('101'))