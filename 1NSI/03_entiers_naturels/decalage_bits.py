## Importation des modules


## Définition des fonctions


## Programme principal

nombre_dec = 1504
nombre_bin = 0b1001011
nombre_hex = 0xEA10C4

print(nombre_dec << 1)
print(nombre_dec >> 1)

print(bin(nombre_dec))
print(bin(nombre_dec << 1))
print(bin(nombre_dec >> 1))
print()

print(nombre_dec << 2)
print(nombre_dec >> 2)
print(bin(nombre_dec))
print(bin(nombre_dec << 2))
print(bin(nombre_dec >> 2))
print()

print(nombre_dec << 3)
print(nombre_dec >> 3)
print(bin(nombre_dec))
print(bin(nombre_dec << 3))
print(bin(nombre_dec >> 3))
print()

print(67 << 1)
print(67 >> 1)
print(bin(67))
print(bin(67 << 1))
print(bin(67 >> 1))
print()